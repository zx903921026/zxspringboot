package com.zx.springbootmybatis;

import com.zx.springbootmybatis.mapper.StudentDOMapper;
import com.zx.springbootmybatis.model.StudentDO;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

@Slf4j
@SpringBootTest
public class CatchTest {
    @Resource
    private SqlSessionFactory sqlSessionFactory;
    @Autowired
    private StudentDOMapper orderMapper;
    @Test
    public void showDefaultCacheConfiguration() {
        System.out.println("一级缓存范围: " + sqlSessionFactory.getConfiguration().getLocalCacheScope());
        System.out.println("二级缓存是否被启用: " + sqlSessionFactory.getConfiguration().isCacheEnabled());
    }

    //不使用一级缓存  因为每次查询都是一个session
    @Test
    void userFirstCache() {
        for (int i = 0; i < 3; i++) {
            StudentDO order = orderMapper.selectByPrimaryKey(1);
            log.info("订单信息：{}", order);
        }
    }
    //使用一级缓存此时，我们发现一级缓存又生效了。而前文提到的乐观锁重试的Bug就是由于在此场景下使用了一级缓存，查询不到最新的数据库数据导致的。此处也是大家在使用的过程中需要留意的。
    //
    //实践中，将Mybatis和Spring进行整合开发，事务控制在service中。如果是执行两次service调用查询相同的用户信息，不走一级缓存，因为Service方法结束，SqlSession就关闭，一级缓存就清空。
    @Transactional
    @Test
    void userFirstCache3() {
        for (int i = 0; i < 3; i++) {
            StudentDO order = orderMapper.selectByPrimaryKey(1);
            log.info("订单信息：{}", order);
        }
    }
    //      1、为什么开启事务
    //    由于使用了数据库连接池，默认每次查询完之后自动commite，这就导致两次查询使用的不是同一个sqlSessioin，根据一级缓存的原理，它将永远不会生效。
    //    当我们开启了事务，两次查询都在同一个sqlSession中，从而让第二次查询命中了一级缓存。读者可以自行关闭事务验证此结论。
    //            2、两种一级缓存模式
    //    一级缓存的作用域有两种：session（默认）和statment，可通过设置local-cache-scope 的值来切换，默认为session。
    //    二者的区别在于session会将缓存作用于同一个sqlSesson，而statment仅针对一次查询，所以，local-cache-scope: statment可以理解为关闭一级缓存。


    //使用一级缓存
    @Test
    void userFirstCache2() {
        SqlSession sqlSession = sqlSessionFactory.openSession();
        StudentDOMapper orderMapper = sqlSession.getMapper(StudentDOMapper.class);
        for (int i = 0; i < 3; i++) {
            StudentDO order = orderMapper.selectByPrimaryKey(1);
            log.info("订单信息：{}", order);
        }
    }

//开启二级缓存
    //    mybatis.configuration.cache-enabled=true

    /**
     * 二级缓存的开启
     * 在上面的Configuration类中我们已经看到默认开启了二级缓存，此开启操作可以通过在application中进行开启或关闭（false）：
     *
     * mybatis.configuration.cache-enabled=true
     * 当然，也可以在SqlMapConfig.xml中加入：
     *
     * <setting name="cacheEnabled"value="true"/>
     * 来开启。
     *
     * 此时只是完成了二级缓存的全局开关，但并没有针对具体的Mapper生效。如果需要对指定的Mapper使用二级缓存，还需要在对应的xml文件中配置如下内容：
     *
     * <mapper namespace="com.secbro.mapper.OrderMapper" >
     * <cache/>
     * <!--省略其他内容-->
     * </mapper>
     * 此时，该namespace下的Mapper便开启了二级缓存。
     *
     * 二级缓存实例
     * 二级缓存需要查询结果映射的pojo对象实现java.io.Serializable接口。如果存在父类、成员pojo都需要实现序列化接口。否则，执行的过程中会直接报错。
     *
     * 此时，Order类实现如下：
     *
     * @Data
     * public class Order implements Serializable {
     *
     *     private int id;
     *
     *     private String orderNo;
     *
     *     private int amount;
     * }
     * 由于二级缓存数据存储介质多种多样，不一定在内存有可能是硬盘或者远程服务器。所以，pojo类实现序列化接口是为了将缓存数据取出执行反序列化操作。
     *
     * 下面看一下具体的单元测试：
     *
     * @Test
     * void userSecondCache() {
     *     for (int i = 0; i < 3; i++) {
     *         Order order = orderService.findById(1);
     *         log.info("订单信息：{}", order);
     *     }
     * }
     * 由于开启了二级缓存，我们直接使用service进行查询，就可以发现缓存已经生效了。
     *
     * image
     *
     * 在图中我们可以看到，还打印出了命中缓存的概率为：0.5。
     *
     * 禁用指定方法的二级缓存
     * 由于cache是针对整个Mapper中的查询方法的，因此当某个方法不需要缓存时，可在对应的select标签中添加useCache值为false来禁用二级缓存。
     *
     * <select id="findById" parameterType="int" resultMap="BaseResultMap" useCache="false">
     */
    @Test
    void userFirstCache4() {
        for (int i = 0; i < 3; i++) {
            StudentDO order = orderMapper.selectByPrimaryKey(1);
            log.info("订单信息：{}", order);
        }
    }


//    多表联查二级缓存
//    接下来演示多表联查的二级缓存，user表left join user_order表 on user.id = user_order.user_id
//    我们考虑这样一种情况，该联查执行两次，第二次联查前更新user_order表，如果只使用cache配置，将会查不到更新的user_orderxi，因为两个mapper.xml的作用域不同，要想合到一个作用域，就需要用到cache-ref
//    userOrderMapper.xml
//            <cache></cache>
//    userMapper.xml
//
//            <cache-ref namespace="com.zhengxl.mybatiscache.mapper.UserOrderMapper"/>






//    mybatis默认的session级别一级缓存，由于springboot中默认使用了hikariCP，所以基本没用，需要开启事务才有用。但一级缓存作用域仅限同一sqlSession内，无法感知到其他sqlSession的增删改，所以极易产生脏数据
//    二级缓存可通过cache-ref让多个mapper.xml共享同一namespace，从而实现缓存共享，但多表联查时配置略微繁琐。
//    所以生产环境建议将一级缓存设置为statment级别（即关闭一级缓存），如果有必要，可以开启二级缓存





    }
