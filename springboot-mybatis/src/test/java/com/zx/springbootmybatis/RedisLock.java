package com.zx.springbootmybatis;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.data.redis.core.script.DefaultRedisScript;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

@SpringBootTest
public class RedisLock {
    @Autowired
    private RedisTemplate redisTemplate;


    @Test
    public void test(){
//        Boolean lock = redisTemplate.opsForValue().setIfAbsent("key", "true",5, TimeUnit.SECONDS);
//
////        Boolean flag=RedisUtil.getAndTryLock("1");
//        System.out.println(lock);
        redisTemplate.opsForValue().set("name","zx");

        String name = (String) redisTemplate.opsForValue().get("name");
        System.out.println(name);
    }



    @Test
    public void getCatalogJsonDbWithRedisLock() {
        String uuid = UUID.randomUUID().toString();
        ValueOperations<String, String> ops = redisTemplate.opsForValue();
        //为当前锁设置唯一的uuid，只有当uuid相同时才会进行删除锁的操作
        Boolean lock = ops.setIfAbsent("lock", uuid,5, TimeUnit.SECONDS);
        if (lock) {

            String lockValue = ops.get("lock");
            if (lockValue.equals(uuid)) {
                try {
                    Thread.sleep(6000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                redisTemplate.delete("lock");
            }
//            return categoriesDb;
        }else {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            getCatalogJsonDbWithRedisLock();
        }
    }
}
