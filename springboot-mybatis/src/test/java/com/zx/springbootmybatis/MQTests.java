package com.zx.springbootmybatis;

import com.zx.springbootmybatis.mq.delayMq.DelayProducer;
import com.zx.springbootmybatis.mq.direct.provider.ProviverDirect;
import com.zx.springbootmybatis.mq.fanout.provider.LianweiFanoutProvider;
import com.zx.springbootmybatis.mq.topic.provider.ProviverTopicProduct;
import com.zx.springbootmybatis.mq.topic.provider.ProviverTopicUser;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class MQTests {

    @Autowired
    ProviverDirect proviverDirect;
    @Test
    void contextLoads() {
        proviverDirect.send("hello");
        System.out.println("发送成功");
    }




    @Autowired
    ProviverTopicProduct proviverTopicProduct;
    @Autowired
    ProviverTopicUser proviverTopicUser;
    @Autowired
    DelayProducer delayProducer;
    @Test
    void topicSend() {
        proviverTopicProduct.send("product send");
        proviverTopicUser.send("user end");
        System.out.println("发送成功");
    }
    @Test
    void delaySend() {
        delayProducer.sendEmail();
    }


    @Autowired
    LianweiFanoutProvider fanoutProvider;
    @Test
    void fanoutSend() {
        fanoutProvider.send("{\n" +
                "  \"uuid\" : \"product-label_20210901111635\",\n" +
                "  \"type\" : \"MQ_SEND\",\n" +
                "  \"data\" : {\n" +
                "    \"@class\" : \"com.amway.bpaas.infrastructure.api.kinesis.KinesisMessage\",\n" +
                "    \"sender\" : \"WM\",\n" +
                "    \"timestamp\" : \"2021-09-01 11:16:37:729\",\n" +
                "    \"messageId\" : \"product-label_20210901111635\",\n" +
                "    \"messageType\" : \"product-label\",\n" +
                "    \"messageDetail\" : \"{\\\"regionCode\\\":\\\"130\\\",\\\"bizCode\\\":\\\"INT\\\",\\\"useCase\\\":\\\"\\\",\\\"scenario\\\":\\\"\\\",\\\"language\\\":\\\"\\\",\\\"timeZone\\\":\\\"GMT+8\\\",\\\"extendProperty\\\":\\\"\\\",\\\"updateTime\\\":\\\"20210901111635\\\",\\\"sourceFrom\\\":\\\"WM\\\",\\\"itemCodes\\\":[\\\"2029\\\",\\\"2644\\\"],\\\"tagCode\\\":\\\"AO\\\",\\\"channelCode\\\":\\\"DEFAULT\\\",\\\"tagRole\\\":\\\"\\\"}\"\n" +
                "  }\n" +
                "}");

        System.out.println("fanoutProvider发送成功");
    }


}
