package com.zx.springbootmybatis;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication//开启spring配置
@MapperScan(basePackages = {"com.zx.springbootmybatis.mapper"})
public class SpringbootMybatisApplication {

    public static void main(String[] args) {
        //获取spring容器
        ConfigurableApplicationContext applicationContext = SpringApplication.run(SpringbootMybatisApplication.class, args);
//        applicationContext.getBean
    }

}
