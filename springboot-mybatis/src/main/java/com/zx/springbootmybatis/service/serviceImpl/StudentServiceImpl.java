package com.zx.springbootmybatis.service.serviceImpl;

import com.zx.springbootmybatis.mapper.StudentDOMapper;
import com.zx.springbootmybatis.model.StudentDO;
import com.zx.springbootmybatis.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

@Service
public class StudentServiceImpl implements StudentService {
    @Resource
    StudentDOMapper studentDOMapper;
    @Override
    public StudentDO getstudentById(Integer id) {
        StudentDO studentDO= studentDOMapper.selectByPrimaryKey(id);
        return studentDO;
    }
    @Transactional
    @Override
    public int updateStudent(StudentDO student) {
        int i = studentDOMapper.updateByPrimaryKeySelective(student);
//        int a=10/0;
        return i;
    }
}
