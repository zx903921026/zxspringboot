package com.zx.springbootmybatis.service;

import com.zx.springbootmybatis.model.StudentDO;

public interface StudentService {
    public StudentDO getstudentById(Integer id);

    int updateStudent(StudentDO student);
}
