package com.zx.springbootmybatis.config;

import com.zx.springbootmybatis.filter.Myfilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration//配置类  相当于XML配置
public class FilterConfig implements WebMvcConfigurer {
//    @Override
//    public void addInterceptors(InterceptorRegistry registry) {
//        String[] addPathPatterns={
//            "/user/**"
//        };
//        //
//        String[] excludePathPatterns={
//                "/user/out"
//        };
//        registry.addInterceptor(new UserFilter()).addPathPatterns(addPathPatterns).excludePathPatterns(excludePathPatterns);
////        WebMvcConfigurer.super.addInterceptors(registry);
//    }

    @Bean
    public FilterRegistrationBean myFilterBean(){
        //注册过滤器
        FilterRegistrationBean filterRegistrationBean=new FilterRegistrationBean(new Myfilter());
        //添加过滤器路径
        filterRegistrationBean.addUrlPatterns("/user/*");
        return filterRegistrationBean;
    }

}
