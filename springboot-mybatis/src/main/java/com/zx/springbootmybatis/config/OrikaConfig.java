package com.zx.springbootmybatis.config;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.converter.BidirectionalConverter;
import ma.glasnost.orika.metadata.Type;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
 
import javax.annotation.PostConstruct;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
 
@Configuration
public class OrikaConfig {
 
	@Bean
	public MapperFactoryBean loadFactory(){
		return new MapperFactoryBean();
	}
 
	@Bean
	public MapperFacade loadMapperFacade(MapperFactory factory){
		return factory.getMapperFacade();
	}
 
	@Autowired
	private MapperFactory mapperFactory;
 
	/**
	 * 解决orika映射LocalDateTime报错问题
	 */
	@PostConstruct
	public void init() {
		mapperFactory.getConverterFactory().registerConverter(new LocalDateTimeConverter());
		mapperFactory.getConverterFactory().registerConverter(new LocalDateConverter());
		mapperFactory.getConverterFactory().registerConverter(new LocalTimeConverter());
	}
	private class LocalDateTimeConverter extends BidirectionalConverter<LocalDateTime, LocalDateTime> {
		@Override
		public LocalDateTime convertTo(LocalDateTime source, Type<LocalDateTime> destinationType) {
			return LocalDateTime.from(source);
		}
		@Override
		public LocalDateTime convertFrom(LocalDateTime source, Type<LocalDateTime> destinationType) {
			return LocalDateTime.from(source);
		}
	}
	private class LocalDateConverter extends BidirectionalConverter<LocalDate, LocalDate> {
		@Override
		public LocalDate convertTo(LocalDate source, Type<LocalDate> destinationType) {
			return LocalDate.from(source);
		}
		@Override
		public LocalDate convertFrom(LocalDate source, Type<LocalDate> destinationType) {
			return LocalDate.from(source);
		}
	}
	private class LocalTimeConverter extends BidirectionalConverter<LocalTime, LocalTime> {
		@Override
		public LocalTime convertTo(LocalTime source, Type<LocalTime> destinationType) {
			return LocalTime.from(source);
		}
		@Override
		public LocalTime convertFrom(LocalTime source, Type<LocalTime> destinationType) {
			return LocalTime.from(source);
		}
	}
 
}
