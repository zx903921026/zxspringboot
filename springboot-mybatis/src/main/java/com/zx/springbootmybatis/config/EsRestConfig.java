//package com.zx.springbootmybatis.config;
//
//import lombok.extern.slf4j.Slf4j;
//import org.apache.http.HttpHost;
//import org.apache.http.auth.AuthScope;
//import org.apache.http.auth.UsernamePasswordCredentials;
//import org.apache.http.client.CredentialsProvider;
//import org.apache.http.client.config.RequestConfig;
//import org.apache.http.impl.client.BasicCredentialsProvider;
//import org.apache.http.impl.nio.client.HttpAsyncClientBuilder;
//import org.apache.http.impl.nio.reactor.IOReactorConfig;
//import org.elasticsearch.client.RestClient;
//import org.elasticsearch.client.RestClientBuilder;
//import org.elasticsearch.client.RestHighLevelClient;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
//
///**
// * @author Zhi.Dong
// * @date 2020-3-31
// */
//@Configuration
//@Slf4j
//public class EsRestConfig {
//
//    @Value("${elasticsearch.rest.host}")
//    private String host;
//    @Value("${elasticsearch.rest.port:9200}")
//    private Integer port;
//    @Value("${elasticsearch.rest.max-thread:1000}")
//    private Integer maxThread;
//    @Value("${elasticsearch.rest.io-reactor:1}")
//    private Integer ioReactor;
//    @Value("${elasticsearch.rest.rcvBufSize:0}")
//    private Integer rcvBufSize;
//    @Value("${elasticsearch.rest.soKeepAlive:true}")
//    private Boolean soKeepAlive;
//    @Value("${elasticsearch.rest.ssl:false}")
//    private Boolean ssl;
//    @Value("${elasticsearch.rest.username:}")
//    private String username;
//    @Value("${elasticsearch.rest.auth:}")
//    private String auth;
//    @Value("${elasticsearch.rest.conn-time-out:15000}")
//    private Integer connectionTimeOut;
//    @Value("${elasticsearch.rest.socket-time-out:15000}")
//    private Integer socketTimeOut;
//    @Value("${elasticsearch.rest.request-time-out:15000}")
//    private Integer requestTimeOut;
//
//    @Bean
//    RestHighLevelClient restHighLevelClient() {
//        String scheme = "http";
//        //支持ssl
//        if (Boolean.TRUE.equals(ssl)) {
//            scheme = "https";
//        }
//        /** 用户认证对象 */
//        final CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
//        /** 设置账号密码 */
//        credentialsProvider.setCredentials(AuthScope.ANY,
//                new UsernamePasswordCredentials(username, auth));
//        RestClientBuilder restClientBuilder = RestClient.builder(
//                new HttpHost(host, port, scheme));
//        log.info("restHighLevelClient, connect to {}:{}", host, port);
//        // 异步httpclient连接数配置
//        restClientBuilder.setHttpClientConfigCallback(new RestClientBuilder.HttpClientConfigCallback() {
//            @Override
//            public HttpAsyncClientBuilder customizeHttpClient(
//                    HttpAsyncClientBuilder httpClientBuilder) {
//                // 如果开启了用户名密码验证，则需要加上
//                httpClientBuilder.setDefaultCredentialsProvider(credentialsProvider);
//                httpClientBuilder.setMaxConnTotal(maxThread);
//                httpClientBuilder.setMaxConnPerRoute(maxThread);
//                final IOReactorConfig ioReactorConfig = IOReactorConfig.custom()
//                        .setIoThreadCount(ioReactor)
//                        .setRcvBufSize(rcvBufSize)
//                        .setSoKeepAlive(soKeepAlive)
//                        .build();
//                httpClientBuilder.setDefaultIOReactorConfig(
//                        ioReactorConfig);
//                return httpClientBuilder;
//            }
//        });
//        // 异步httpclient连接延时配置
//        restClientBuilder.setRequestConfigCallback(new RestClientBuilder.RequestConfigCallback() {
//            @Override
//            public RequestConfig.Builder customizeRequestConfig(RequestConfig.Builder requestConfigBuilder) {
//                requestConfigBuilder.setConnectTimeout(connectionTimeOut);
//                requestConfigBuilder.setSocketTimeout(socketTimeOut);
//                requestConfigBuilder.setConnectionRequestTimeout(requestTimeOut);
//                return requestConfigBuilder;
//            }
//        });
//        RestHighLevelClient client = new RestHighLevelClient(restClientBuilder);
//        return client;
//    }
//
//    @Bean(name = "elasticsearchTemplate")
//    ElasticsearchRestTemplate elasticsearchRestTemplate(@Autowired RestHighLevelClient restHighLevelClient) {
//        return new ElasticsearchRestTemplate(restHighLevelClient);
//    }
//}
