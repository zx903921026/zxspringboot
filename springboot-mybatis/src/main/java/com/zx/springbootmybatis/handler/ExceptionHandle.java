package com.zx.springbootmybatis.handler;

import com.zx.springbootmybatis.constants.Constants;
import com.zx.springbootmybatis.exception.CommonException;
import com.zx.springbootmybatis.exception.IResultCode;
import com.zx.springbootmybatis.model.ResultResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 异常统一处理
 */
@Slf4j
@ControllerAdvice
public class ExceptionHandle {
    @Autowired
    private Environment environment;

    @ExceptionHandler(value = CommonException.class)
    @ResponseBody
    public ResultResponse handleException(CommonException e) {
        return errorResult(e.getResultCode().getErrorCode(),getErrorMessage(e.getResultCode()),e.getData());
    }
    public static <T> ResultResponse<T> errorResult(String code, String message,T data) {
        ResultResponse<T> resultDTO = new ResultResponse();
        resultDTO.setCode(code);
        resultDTO.setMessage(message);
        resultDTO.setSuccess(false);
        resultDTO.setData(data);
        return resultDTO;
    }
    private String getErrorMessage(IResultCode iResultCode){
        String errorMessage=environment.getProperty(Constants.Response.Message.PREFIX+iResultCode.getErrorCode());
        return StringUtils.isEmpty(errorMessage)?iResultCode.getErrorMessage():errorMessage;
    }

}
