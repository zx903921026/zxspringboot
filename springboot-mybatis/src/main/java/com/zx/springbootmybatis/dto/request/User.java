package com.zx.springbootmybatis.dto.request;

import org.hibernate.validator.constraints.Length;
import javax.validation.constraints.NotBlank;

import javax.validation.constraints.Pattern;

public class User {
//    @Length(min=,max=)	检查所属的字段的长度是否在min和max之间，只能用于字符串
//    @Range(min=,max=,message=)	被注释的元素必须在合适的范围内
//    @Max	该字段的值只能小于或等于该值
//    @Min	该字段的值只能大于或等于该值
//    @NotNull	不能为null
//    @NotBlank	不能为空，检查时会将空格忽略
//    @NotEmpty	不能为空，这里的空是指空字符串
//    @Pattern(regex=,flag=)	被注释的元素必须符合指定的正则表达式

//    需要搭配在Controller中搭配@Validated或@Valid注解一起使用，@Validated和@Valid注解区别不是很大，一般情况下任选一个即可，区别如下：
//    @PostMapping
//    public ResponseVO createDataSet(@Valid @RequestBody DataSetSaveVO dataSetVO) {
//        return ResponseUtil.success(dataSetService.saveDataSet(dataSetVO));
//    }

    //唯一标识符为空
    @NotBlank(message = "user uuid is empty")
    //用户名称只能是字母和数字
    @Pattern(regexp = "^[a-z0-9]+$", message = "user names can only be alphabetic and numeric")
    @Length(max = 48, message = "user uuid length over 48 byte")
    private String userUuid;

    //数据集名称只能是字母和数字
    @Pattern(regexp = "^[A-Za-z0-9]+$", message = "data set names can only be letters and Numbers")
    //文件名称过长
    @Length(max = 48, message = "file name too long")
    //文件名称为空
    @NotBlank(message = "file name is empty")
    private String name;

    //数据集描述最多为256字节
    @Length(max = 256, message = "data set description length over 256 byte")
    //数据集描述为空
    @NotBlank(message = "data set description is null")
    private String description;
}