package com.zx.springbootmybatis.dto.response;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

@Data
public class StudentDTO {
    @Excel(name = "id", width = 20)
    private Integer id;
    @Excel(name = "name", width = 20)
    private String name;
    @Excel(name = "age", width = 20)
    private String age;

    @Excel(name = "createTime", width = 20)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;
    @Excel(name = "updateTime", width = 20)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;
}
