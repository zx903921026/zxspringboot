package com.zx.springbootmybatis.dto;

import com.alibaba.excel.annotation.ExcelProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;


@Data
public class ExcelDTO {
    @ExcelProperty(value = "列0",index = 0)
    private String column0;
    @ExcelProperty(value = "列1",index = 1)
    private String column1;
    @ExcelProperty(value = "列2",index = 2)
    private String column2;
    @ExcelProperty(value = "列3",index = 3)
    private String column3;
    @ExcelProperty(value = "列4",index = 4)
    private String column4;
    @ExcelProperty(value = "列5",index = 5)
    private String column5;



//    private Integer id;
//
//    private String type;
//
//    private Long sopId;
//
//    private Long ada;
//    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
//    private LocalDateTime effectiveTime;
//    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
//    private LocalDateTime createTime;
//
//    private Integer auditStatus;
//    private Integer isDelete;
//
//    private String performanceRecord;
//
//    private String message;
//    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
//    private LocalDateTime updateTime;
//
//
//    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
//    private LocalDateTime startTime;
//    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
//    private LocalDateTime endTime;
//    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
//    private LocalDateTime effectiveTimeStart;
//    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
//    private LocalDateTime effectiveTimeEnd;
//
//    private String createUser;
//
//    private String createUserId;
//
//    @ApiModelProperty(value = "每页显示行数", name = "pageSize", required = false)
//    private Integer pageSize;
//
//    @ApiModelProperty(value = "页码", name = "pageNum", required = false)
//    private Integer pageNum;
    //0正常 1错误
//    private Integer errorFlag=0;
//
//    private List<String> errormeaasges =new ArrayList<>();


}