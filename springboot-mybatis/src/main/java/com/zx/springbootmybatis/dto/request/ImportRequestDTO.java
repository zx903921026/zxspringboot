package com.zx.springbootmybatis.dto.request;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;

/**
 * @Author: ly
 * @Title: importLandingRequestDTO
 * @Description
 * @Date: 2020/11/18 10:08
 */
@Data
public class ImportRequestDTO {
    @NotNull(message = "文件不可为空")
    MultipartFile file;
}
