package com.zx.springbootmybatis.dto.response;

import lombok.Data;

import java.util.List;

@Data
public class ImportReturnDTO {
    private Integer successSum;

    private Integer errorSum;

    private List errorList;
}