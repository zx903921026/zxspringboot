package com.zx.springbootmybatis.mapper;

import com.zx.springbootmybatis.model.ManualRefund;
import com.zx.springbootmybatis.model.ManualRefundExample;
import java.util.List;

public interface ManualRefundMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ManualRefund record);

    int insertSelective(ManualRefund record);

    List<ManualRefund> selectByExample(ManualRefundExample example);

    ManualRefund selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ManualRefund record);

    int updateByPrimaryKey(ManualRefund record);
}