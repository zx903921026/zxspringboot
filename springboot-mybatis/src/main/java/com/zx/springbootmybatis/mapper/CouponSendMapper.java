package com.zx.springbootmybatis.mapper;

import com.zx.springbootmybatis.model.CouponSend;
import com.zx.springbootmybatis.model.CouponSendExample;
import java.util.List;

public interface CouponSendMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(CouponSend record);

    int insertSelective(CouponSend record);

    List<CouponSend> selectByExample(CouponSendExample example);

    CouponSend selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(CouponSend record);

    int updateByPrimaryKey(CouponSend record);
}