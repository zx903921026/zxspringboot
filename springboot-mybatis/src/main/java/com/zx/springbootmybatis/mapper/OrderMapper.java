package com.zx.springbootmybatis.mapper;

import com.zx.springbootmybatis.model.Order;

public interface OrderMapper {
    int insert(Order record);

    int insertSelective(Order record);
}