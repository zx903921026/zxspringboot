package com.zx.springbootmybatis.es.esModel;

import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.io.Serializable;

@Data
@Accessors(chain = true)
public class MultiLanguageModel implements Serializable {

    /**
     * code
     */
    @Field(type = FieldType.Keyword)
    private String code;

    /**
     * name
     */
    @Field(type = FieldType.Text)
    private String name;

    /**
     * nameForSort
     */
    @Field(type = FieldType.Keyword)
    private String nameForSort;

    /**
     * language
     */
    @Field(type = FieldType.Keyword)
    private String language;

}
