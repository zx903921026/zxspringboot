package com.zx.springbootmybatis.es.service.impl;

import com.zx.springbootmybatis.es.dao.EmployeeDAO;
import com.zx.springbootmybatis.es.esModel.EmployeeBean;
import com.zx.springbootmybatis.es.service.EsQueryService;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.search.*;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.common.geo.GeoPoint;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.*;
import org.elasticsearch.index.reindex.BulkByScrollResponse;
import org.elasticsearch.index.reindex.DeleteByQueryRequest;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.range.Range;
import org.elasticsearch.search.aggregations.metrics.cardinality.Cardinality;
import org.elasticsearch.search.aggregations.metrics.stats.extended.ExtendedStats;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class EsQueryServiceImpl implements EsQueryService {

    @Resource
    private ElasticsearchRestTemplate esTemplate;


    @Resource
    EmployeeDAO employeeDAO;
    @Resource
    ElasticsearchRepository elasticsearchRepository;

    public void termsQuery2() throws IOException {



//        TermQueryBuilder queryBuilder = QueryBuilders.termQuery("name", "zx");
//        //分页
//        Sort sort = Sort.by(Sort.Direction.DESC, "id");
//        PageRequest pageRequest = PageRequest.of(0, 2, sort);
//        Page<EmployeeBean> search = employeeDAO.search(queryBuilder, pageRequest);
//        Iterable<EmployeeBean> search2 = elasticsearchRepository.search(queryBuilder);
    }


    @Value("product_index1231")
    String index;

    //terms查询  查多个
    public void termsQuery() throws IOException {
        SearchRequest request = new SearchRequest(index);
        SearchSourceBuilder builder = new SearchSourceBuilder();
        builder.query(QueryBuilders.termsQuery("itemCode", "40214", "40239"));
        request.source(builder);
        //3. 执行查询
        SearchResponse resp = esTemplate.getClient().search(request, RequestOptions.DEFAULT);
        //4. 输出_source
        for (SearchHit hit : resp.getHits().getHits()) {
            System.out.println(hit.getSourceAsMap());
        }
    }


    //term查询  查单个
    public void termQuery() throws IOException {
        SearchRequest request = new SearchRequest(index);
        SearchSourceBuilder builder = new SearchSourceBuilder();
        builder.query(QueryBuilders.termQuery("itemCode", "40214"));
        builder.from(0);
        builder.size(5);
        request.source(builder);
        //3. 执行查询
        SearchResponse resp = esTemplate.getClient().search(request, RequestOptions.DEFAULT);
        //4. 输出_source
        for (SearchHit hit : resp.getHits().getHits()) {
            System.out.println(hit.getSourceAsMap());
        }
    }

    //一个field做检索  针对对各field进行检索
    public void multiMatchQuery() throws IOException {
        //1. 创建Request
        SearchRequest request = new SearchRequest(index);
//        request.types(type);

        //2. 指定查询条件
        SearchSourceBuilder builder = new SearchSourceBuilder();
        //-----------------------------------------------
        builder.query(QueryBuilders.multiMatchQuery("安利（中国）日用品有限公司", "companyName", "productName"));
        //-----------------------------------------------
        request.source(builder);
        //3. 执行查询
        SearchResponse resp = esTemplate.getClient().search(request, RequestOptions.DEFAULT);

        //4. 输出结果
        for (SearchHit hit : resp.getHits().getHits()) {
            System.out.println(hit.getSourceAsMap());
        }
    }

    //是否匹配
    public void booleanMatchQuery() throws IOException {
        //1. 创建Request
        SearchRequest request = new SearchRequest(index);

        //2. 指定查询条件
        SearchSourceBuilder builder = new SearchSourceBuilder();
        //-----------------------------------------------                               选择AND或者OR
        builder.query(QueryBuilders.matchQuery("itemCode", "50002 40302").operator(Operator.OR));
        //-----------------------------------------------
        request.source(builder);
        //3. 执行查询
        SearchResponse resp = esTemplate.getClient().search(request, RequestOptions.DEFAULT);

        //4. 输出结果
        for (SearchHit hit : resp.getHits().getHits()) {
            System.out.println(hit.getSourceAsMap());
        }
    }

    public void matchQuery() throws IOException {
        //1. 创建Request
        SearchRequest request = new SearchRequest(index);

        //2. 指定查询条件
        SearchSourceBuilder builder = new SearchSourceBuilder();
        //-----------------------------------------------
        builder.query(QueryBuilders.matchQuery("itemCode", "5000"));
        //-----------------------------------------------
        request.source(builder);
        //3. 执行查询
        SearchResponse resp = esTemplate.getClient().search(request, RequestOptions.DEFAULT);

        //4. 输出结果
        for (SearchHit hit : resp.getHits().getHits()) {
            System.out.println(hit.getSourceAsMap());
        }
    }

    public void matchAllQuery() throws IOException {
        //1. 创建Request
        SearchRequest request = new SearchRequest(index);

        //2. 指定查询条件
        SearchSourceBuilder builder = new SearchSourceBuilder();
        builder.query(QueryBuilders.matchAllQuery());
        builder.size(20);           // ES默认只查询10条数据，如果想查询更多，添加size
        request.source(builder);

        //3. 执行查询
        SearchResponse resp = esTemplate.getClient().search(request, RequestOptions.DEFAULT);

        //4. 输出结果
        for (SearchHit hit : resp.getHits().getHits()) {
            System.out.println(hit.getSourceAsMap());
        }
        System.out.println(resp.getHits().getHits().length);
    }

    //正则查询
    public void findByRegexp() throws IOException {
        //1. 创建SearchRequest
        SearchRequest request = new SearchRequest(index);

        //2. 指定查询条件
        SearchSourceBuilder builder = new SearchSourceBuilder();
        //----------------------------------------------------------
        builder.query(QueryBuilders.regexpQuery("mobile", "139[0-9]{8}"));
        //----------------------------------------------------------
        request.source(builder);

        //3. 执行
        SearchResponse resp = esTemplate.getClient().search(request, RequestOptions.DEFAULT);

        //4. 输出结果
        for (SearchHit hit : resp.getHits().getHits()) {
            System.out.println(hit.getSourceAsMap());
        }
    }

    //范围查询
    public void findByRange() throws IOException {
        //1. 创建SearchRequest
        SearchRequest request = new SearchRequest(index);

        //2. 指定查询条件
        SearchSourceBuilder builder = new SearchSourceBuilder();
        //----------------------------------------------------------
        builder.query(QueryBuilders.rangeQuery("fee").lte(10).gte(5));
        //----------------------------------------------------------
        request.source(builder);

        //3. 执行
        SearchResponse resp = esTemplate.getClient().search(request, RequestOptions.DEFAULT);

        //4. 输出结果
        for (SearchHit hit : resp.getHits().getHits()) {
            System.out.println(hit.getSourceAsMap());
        }
    }


    public void findByFuzzy() throws IOException {
        //1. 创建SearchRequest
        SearchRequest request = new SearchRequest(index);

        //2. 指定查询条件
        SearchSourceBuilder builder = new SearchSourceBuilder();
        //----------------------------------------------------------
        builder.query(QueryBuilders.fuzzyQuery("province", "盒马先生").prefixLength(0));
        //----------------------------------------------------------
        request.source(builder);

        //3. 执行
        SearchResponse resp = esTemplate.getClient().search(request, RequestOptions.DEFAULT);

        //4. 输出结果
        for (SearchHit hit : resp.getHits().getHits()) {
            System.out.println(hit.getSourceAsMap());
        }
    }

    public void findByPrefix() throws IOException {
        //1. 创建SearchRequest
        SearchRequest request = new SearchRequest(index);

        //2. 指定查询条件
        SearchSourceBuilder builder = new SearchSourceBuilder();
        //----------------------------------------------------------
        builder.query(QueryBuilders.prefixQuery("corpName", "盒马"));
        //----------------------------------------------------------
        request.source(builder);

        //3. 执行
        SearchResponse resp = esTemplate.getClient().search(request, RequestOptions.DEFAULT);

        //4. 输出结果
        for (SearchHit hit : resp.getHits().getHits()) {
            System.out.println(hit.getSourceAsMap());
        }
    }

    public void findByIds() throws IOException {
        //1. 创建SearchRequest
        SearchRequest request = new SearchRequest(index);
        //2. 指定查询条件
        SearchSourceBuilder builder = new SearchSourceBuilder();
        //----------------------------------------------------------
        builder.query(QueryBuilders.idsQuery().addIds("1", "2", "3"));
        //----------------------------------------------------------
        request.source(builder);

        //3. 执行
        SearchResponse resp = esTemplate.getClient().search(request, RequestOptions.DEFAULT);

        //4. 输出结果
        for (SearchHit hit : resp.getHits().getHits()) {
            System.out.println(hit.getSourceAsMap());
        }
    }


    public void findById() throws IOException {
        //1. 创建GetRequest
        GetRequest request = new GetRequest(index, null, "50002");

        //2. 执行查询
        GetResponse resp = esTemplate.getClient().get(request, RequestOptions.DEFAULT);

        //3. 输出结果
        System.out.println(resp.getSourceAsMap());
    }

    //分页查询
    public void scrollQuery() throws IOException {
        //1. 创建SearchRequest
        SearchRequest request = new SearchRequest(index);

        //2. 指定scroll信息
        request.scroll(TimeValue.timeValueMinutes(1L));

        //3. 指定查询条件
        SearchSourceBuilder builder = new SearchSourceBuilder();
        builder.size(4);
        builder.sort("id", SortOrder.DESC);
        builder.query(QueryBuilders.matchAllQuery());

        request.source(builder);

        //4. 获取返回结果scrollId，source
        SearchResponse resp = esTemplate.getClient().search(request, RequestOptions.DEFAULT);

        String scrollId = resp.getScrollId();
        System.out.println("----------首页---------");
        for (SearchHit hit : resp.getHits().getHits()) {
            System.out.println(hit.getSourceAsMap());
        }


        while (true) {
            //5. 循环 - 创建SearchScrollRequest
            SearchScrollRequest scrollRequest = new SearchScrollRequest(scrollId);

            //6. 指定scrollId的生存时间
            scrollRequest.scroll(TimeValue.timeValueMinutes(1L));

            //7. 执行查询获取返回结果
            SearchResponse scrollResp = esTemplate.getClient().scroll(scrollRequest, RequestOptions.DEFAULT);

            //8. 判断是否查询到了数据，输出
            SearchHit[] hits = scrollResp.getHits().getHits();
            if (hits != null && hits.length > 0) {
                System.out.println("----------下一页---------");
                for (SearchHit hit : hits) {
                    System.out.println(hit.getSourceAsMap());
                }
            } else {
                //9. 判断没有查询到数据-退出循环
                System.out.println("----------结束---------");
                break;
            }
        }


        //10. 创建CLearScrollRequest
        ClearScrollRequest clearScrollRequest = new ClearScrollRequest();

        //11. 指定ScrollId
        clearScrollRequest.addScrollId(scrollId);

        //12. 删除ScrollId
        ClearScrollResponse clearScrollResponse = esTemplate.getClient().clearScroll(clearScrollRequest, RequestOptions.DEFAULT);

        //13. 输出结果
        System.out.println("删除scroll：" + clearScrollResponse.isSucceeded());

    }


    public void deleteByQuery() throws IOException {
        //1. 创建DeleteByQueryRequest
        DeleteByQueryRequest request = new DeleteByQueryRequest(index);

        //2. 指定检索的条件    和SearchRequest指定Query的方式不一样
        request.setQuery(QueryBuilders.rangeQuery("fee").lt(4));

        //3. 执行删除
        BulkByScrollResponse resp = esTemplate.getClient().deleteByQuery(request, RequestOptions.DEFAULT);

        //4. 输出返回结果
        System.out.println(resp.toString());

    }


    public void BoostingQuery() throws IOException {
        //1. 创建SearchRequest
        SearchRequest request = new SearchRequest(index);

        //2. 指定查询条件
        SearchSourceBuilder builder = new SearchSourceBuilder();
        BoostingQueryBuilder boostingQuery = QueryBuilders.boostingQuery(
                QueryBuilders.matchQuery("smsContent", "收货安装"),
                QueryBuilders.matchQuery("smsContent", "王五")
        ).negativeBoost(0.5f);

        builder.query(boostingQuery);
        request.source(builder);

        //3. 执行查询
        SearchResponse resp = esTemplate.getClient().search(request, RequestOptions.DEFAULT);

        //4. 输出结果
        for (SearchHit hit : resp.getHits().getHits()) {
            System.out.println(hit.getSourceAsMap());
        }
    }

    //    复合查询 bool
    public void BoolQuery() throws IOException {
        //1. 创建SearchRequest
        SearchRequest request = new SearchRequest(index);

        //2. 指定查询条件
        SearchSourceBuilder builder = new SearchSourceBuilder();
        BoolQueryBuilder boolQuery = QueryBuilders.boolQuery();
        // # 查询省份为武汉或者北京
        boolQuery.should(QueryBuilders.termQuery("province", "武汉"));
        boolQuery.should(QueryBuilders.termQuery("province", "北京"));
        // # 运营商不是联通
        boolQuery.mustNot(QueryBuilders.termQuery("operatorId", 2));
        // # smsContent中包含中国和平安
        boolQuery.must(QueryBuilders.matchQuery("smsContent", "中国"));
        boolQuery.must(QueryBuilders.matchQuery("smsContent", "平安"));

        builder.query(boolQuery);
        request.source(builder);

        //3. 执行查询
        SearchResponse resp = esTemplate.getClient().search(request, RequestOptions.DEFAULT);

        //4. 输出结果
        for (SearchHit hit : resp.getHits().getHits()) {
            System.out.println(hit.getSourceAsMap());
        }
    }


    public void filter() throws IOException {
        //1. SearchRequest
        SearchRequest request = new SearchRequest(index);

        //2. 查询条件
        SearchSourceBuilder builder = new SearchSourceBuilder();
        BoolQueryBuilder boolQuery = QueryBuilders.boolQuery();
        boolQuery.filter(QueryBuilders.termQuery("corpName", "盒马鲜生"));
        boolQuery.filter(QueryBuilders.rangeQuery("fee").lte(10));

        builder.query(boolQuery);
        request.source(builder);

        //3. 执行查询
        SearchResponse resp = esTemplate.getClient().search(request, RequestOptions.DEFAULT);

        //4. 输出结果
        for (SearchHit hit : resp.getHits().getHits()) {
            System.out.println(hit.getSourceAsMap());
        }

    }

    public void highLightQuery() throws IOException {
        //1. SearchRequest
        SearchRequest request = new SearchRequest(index);

        //2. 指定查询条件（高亮）
        SearchSourceBuilder builder = new SearchSourceBuilder();
        //2.1 指定查询条件
        builder.query(QueryBuilders.matchQuery("smsContent", "途虎养车"));
        //2.2 指定高亮
        HighlightBuilder highlightBuilder = new HighlightBuilder();
        highlightBuilder.field("smsContent", 10)
                .preTags("<font color='red'>")
                .postTags("</font>");
        builder.highlighter(highlightBuilder);

        request.source(builder);

        //3. 执行查询
        SearchResponse resp = esTemplate.getClient().search(request, RequestOptions.DEFAULT);

        //4. 获取高亮数据，输出
        for (SearchHit hit : resp.getHits().getHits()) {
            System.out.println(hit.getHighlightFields().get("smsContent"));
        }
    }


    public void extendedStats() throws IOException {
        //1. 创建SearchRequest
        SearchRequest request = new SearchRequest(index);

        //2. 指定使用的聚合查询方式
        SearchSourceBuilder builder = new SearchSourceBuilder();
        //---------------------------------------------
        builder.aggregation(AggregationBuilders.extendedStats("agg").field("fee"));
        //---------------------------------------------
        request.source(builder);

        //3. 执行查询
        SearchResponse resp = esTemplate.getClient().search(request, RequestOptions.DEFAULT);

        //4. 获取返回结果
        ExtendedStats agg = resp.getAggregations().get("agg");
        double max = agg.getMax();
        double min = agg.getMin();
        System.out.println("fee的最大值为：" + max + "，最小值为：" + min);
    }

    public void range() throws IOException {
        //1. 创建SearchRequest
        SearchRequest request = new SearchRequest(index);

        //2. 指定使用的聚合查询方式
        SearchSourceBuilder builder = new SearchSourceBuilder();
        //---------------------------------------------
        builder.aggregation(AggregationBuilders.range("agg").field("fee")
                .addUnboundedTo(5)
                .addRange(5, 10)
                .addUnboundedFrom(10));
        //---------------------------------------------
        request.source(builder);

        //3. 执行查询
        SearchResponse resp = esTemplate.getClient().search(request, RequestOptions.DEFAULT);

        //4. 获取返回结果
        Range agg = resp.getAggregations().get("agg");
        for (Range.Bucket bucket : agg.getBuckets()) {
            String key = bucket.getKeyAsString();
            Object from = bucket.getFrom();
            Object to = bucket.getTo();
            long docCount = bucket.getDocCount();
            System.out.println(String.format("key：%s，from：%s，to：%s，docCount：%s", key, from, to, docCount));
        }
    }

    public void cardinality() throws IOException {
        //1. 创建SearchRequest
        SearchRequest request = new SearchRequest(index);

        //2. 指定使用的聚合查询方式
        SearchSourceBuilder builder = new SearchSourceBuilder();
        builder.aggregation(AggregationBuilders.cardinality("agg").field("province"));
        request.source(builder);
        //3. 执行查询
        SearchResponse resp = esTemplate.getClient().search(request, RequestOptions.DEFAULT);
        //4. 获取返回结果
        Cardinality agg = resp.getAggregations().get("agg");
        long value = agg.getValue();
        System.out.println(value);
    }


    public void geoPolygon() throws IOException {
        //1. SearchRequest
        SearchRequest request = new SearchRequest(index);

        //2. 指定检索方式
        SearchSourceBuilder builder = new SearchSourceBuilder();
        List<GeoPoint> points = new ArrayList<GeoPoint>();
        points.add(new GeoPoint(39.99878, 116.298916));
        points.add(new GeoPoint(39.972576, 116.29561));
        points.add(new GeoPoint(39.984739, 116.327661));
        builder.query(QueryBuilders.geoPolygonQuery("location", points));
        request.source(builder);

        //3. 执行查询
        SearchResponse resp = esTemplate.getClient().search(request, RequestOptions.DEFAULT);

        //4. 输出结果
        for (SearchHit hit : resp.getHits().getHits()) {
            System.out.println(hit.getSourceAsMap());
        }
    }

}