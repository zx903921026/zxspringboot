package com.zx.springbootmybatis.es.dao;

import com.zx.springbootmybatis.es.esModel.EmployeeBean;
import org.elasticsearch.index.query.TermQueryBuilder;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeeDAO extends ElasticsearchRepository<EmployeeBean,Long> {
}