package com.zx.springbootmybatis.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CouponSendExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public CouponSendExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andCouponTypeIsNull() {
            addCriterion("coupon_type is null");
            return (Criteria) this;
        }

        public Criteria andCouponTypeIsNotNull() {
            addCriterion("coupon_type is not null");
            return (Criteria) this;
        }

        public Criteria andCouponTypeEqualTo(String value) {
            addCriterion("coupon_type =", value, "couponType");
            return (Criteria) this;
        }

        public Criteria andCouponTypeNotEqualTo(String value) {
            addCriterion("coupon_type <>", value, "couponType");
            return (Criteria) this;
        }

        public Criteria andCouponTypeGreaterThan(String value) {
            addCriterion("coupon_type >", value, "couponType");
            return (Criteria) this;
        }

        public Criteria andCouponTypeGreaterThanOrEqualTo(String value) {
            addCriterion("coupon_type >=", value, "couponType");
            return (Criteria) this;
        }

        public Criteria andCouponTypeLessThan(String value) {
            addCriterion("coupon_type <", value, "couponType");
            return (Criteria) this;
        }

        public Criteria andCouponTypeLessThanOrEqualTo(String value) {
            addCriterion("coupon_type <=", value, "couponType");
            return (Criteria) this;
        }

        public Criteria andCouponTypeLike(String value) {
            addCriterion("coupon_type like", value, "couponType");
            return (Criteria) this;
        }

        public Criteria andCouponTypeNotLike(String value) {
            addCriterion("coupon_type not like", value, "couponType");
            return (Criteria) this;
        }

        public Criteria andCouponTypeIn(List<String> values) {
            addCriterion("coupon_type in", values, "couponType");
            return (Criteria) this;
        }

        public Criteria andCouponTypeNotIn(List<String> values) {
            addCriterion("coupon_type not in", values, "couponType");
            return (Criteria) this;
        }

        public Criteria andCouponTypeBetween(String value1, String value2) {
            addCriterion("coupon_type between", value1, value2, "couponType");
            return (Criteria) this;
        }

        public Criteria andCouponTypeNotBetween(String value1, String value2) {
            addCriterion("coupon_type not between", value1, value2, "couponType");
            return (Criteria) this;
        }

        public Criteria andMobileIsNull() {
            addCriterion("mobile is null");
            return (Criteria) this;
        }

        public Criteria andMobileIsNotNull() {
            addCriterion("mobile is not null");
            return (Criteria) this;
        }

        public Criteria andMobileEqualTo(Integer value) {
            addCriterion("mobile =", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileNotEqualTo(Integer value) {
            addCriterion("mobile <>", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileGreaterThan(Integer value) {
            addCriterion("mobile >", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileGreaterThanOrEqualTo(Integer value) {
            addCriterion("mobile >=", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileLessThan(Integer value) {
            addCriterion("mobile <", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileLessThanOrEqualTo(Integer value) {
            addCriterion("mobile <=", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileIn(List<Integer> values) {
            addCriterion("mobile in", values, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileNotIn(List<Integer> values) {
            addCriterion("mobile not in", values, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileBetween(Integer value1, Integer value2) {
            addCriterion("mobile between", value1, value2, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileNotBetween(Integer value1, Integer value2) {
            addCriterion("mobile not between", value1, value2, "mobile");
            return (Criteria) this;
        }

        public Criteria andAdaIsNull() {
            addCriterion("ada is null");
            return (Criteria) this;
        }

        public Criteria andAdaIsNotNull() {
            addCriterion("ada is not null");
            return (Criteria) this;
        }

        public Criteria andAdaEqualTo(Integer value) {
            addCriterion("ada =", value, "ada");
            return (Criteria) this;
        }

        public Criteria andAdaNotEqualTo(Integer value) {
            addCriterion("ada <>", value, "ada");
            return (Criteria) this;
        }

        public Criteria andAdaGreaterThan(Integer value) {
            addCriterion("ada >", value, "ada");
            return (Criteria) this;
        }

        public Criteria andAdaGreaterThanOrEqualTo(Integer value) {
            addCriterion("ada >=", value, "ada");
            return (Criteria) this;
        }

        public Criteria andAdaLessThan(Integer value) {
            addCriterion("ada <", value, "ada");
            return (Criteria) this;
        }

        public Criteria andAdaLessThanOrEqualTo(Integer value) {
            addCriterion("ada <=", value, "ada");
            return (Criteria) this;
        }

        public Criteria andAdaIn(List<Integer> values) {
            addCriterion("ada in", values, "ada");
            return (Criteria) this;
        }

        public Criteria andAdaNotIn(List<Integer> values) {
            addCriterion("ada not in", values, "ada");
            return (Criteria) this;
        }

        public Criteria andAdaBetween(Integer value1, Integer value2) {
            addCriterion("ada between", value1, value2, "ada");
            return (Criteria) this;
        }

        public Criteria andAdaNotBetween(Integer value1, Integer value2) {
            addCriterion("ada not between", value1, value2, "ada");
            return (Criteria) this;
        }

        public Criteria andIdentityTypeIsNull() {
            addCriterion("identity_type is null");
            return (Criteria) this;
        }

        public Criteria andIdentityTypeIsNotNull() {
            addCriterion("identity_type is not null");
            return (Criteria) this;
        }

        public Criteria andIdentityTypeEqualTo(String value) {
            addCriterion("identity_type =", value, "identityType");
            return (Criteria) this;
        }

        public Criteria andIdentityTypeNotEqualTo(String value) {
            addCriterion("identity_type <>", value, "identityType");
            return (Criteria) this;
        }

        public Criteria andIdentityTypeGreaterThan(String value) {
            addCriterion("identity_type >", value, "identityType");
            return (Criteria) this;
        }

        public Criteria andIdentityTypeGreaterThanOrEqualTo(String value) {
            addCriterion("identity_type >=", value, "identityType");
            return (Criteria) this;
        }

        public Criteria andIdentityTypeLessThan(String value) {
            addCriterion("identity_type <", value, "identityType");
            return (Criteria) this;
        }

        public Criteria andIdentityTypeLessThanOrEqualTo(String value) {
            addCriterion("identity_type <=", value, "identityType");
            return (Criteria) this;
        }

        public Criteria andIdentityTypeLike(String value) {
            addCriterion("identity_type like", value, "identityType");
            return (Criteria) this;
        }

        public Criteria andIdentityTypeNotLike(String value) {
            addCriterion("identity_type not like", value, "identityType");
            return (Criteria) this;
        }

        public Criteria andIdentityTypeIn(List<String> values) {
            addCriterion("identity_type in", values, "identityType");
            return (Criteria) this;
        }

        public Criteria andIdentityTypeNotIn(List<String> values) {
            addCriterion("identity_type not in", values, "identityType");
            return (Criteria) this;
        }

        public Criteria andIdentityTypeBetween(String value1, String value2) {
            addCriterion("identity_type between", value1, value2, "identityType");
            return (Criteria) this;
        }

        public Criteria andIdentityTypeNotBetween(String value1, String value2) {
            addCriterion("identity_type not between", value1, value2, "identityType");
            return (Criteria) this;
        }

        public Criteria andEffectiveDateStartIsNull() {
            addCriterion("effective_date_start is null");
            return (Criteria) this;
        }

        public Criteria andEffectiveDateStartIsNotNull() {
            addCriterion("effective_date_start is not null");
            return (Criteria) this;
        }

        public Criteria andEffectiveDateStartEqualTo(Date value) {
            addCriterion("effective_date_start =", value, "effectiveDateStart");
            return (Criteria) this;
        }

        public Criteria andEffectiveDateStartNotEqualTo(Date value) {
            addCriterion("effective_date_start <>", value, "effectiveDateStart");
            return (Criteria) this;
        }

        public Criteria andEffectiveDateStartGreaterThan(Date value) {
            addCriterion("effective_date_start >", value, "effectiveDateStart");
            return (Criteria) this;
        }

        public Criteria andEffectiveDateStartGreaterThanOrEqualTo(Date value) {
            addCriterion("effective_date_start >=", value, "effectiveDateStart");
            return (Criteria) this;
        }

        public Criteria andEffectiveDateStartLessThan(Date value) {
            addCriterion("effective_date_start <", value, "effectiveDateStart");
            return (Criteria) this;
        }

        public Criteria andEffectiveDateStartLessThanOrEqualTo(Date value) {
            addCriterion("effective_date_start <=", value, "effectiveDateStart");
            return (Criteria) this;
        }

        public Criteria andEffectiveDateStartIn(List<Date> values) {
            addCriterion("effective_date_start in", values, "effectiveDateStart");
            return (Criteria) this;
        }

        public Criteria andEffectiveDateStartNotIn(List<Date> values) {
            addCriterion("effective_date_start not in", values, "effectiveDateStart");
            return (Criteria) this;
        }

        public Criteria andEffectiveDateStartBetween(Date value1, Date value2) {
            addCriterion("effective_date_start between", value1, value2, "effectiveDateStart");
            return (Criteria) this;
        }

        public Criteria andEffectiveDateStartNotBetween(Date value1, Date value2) {
            addCriterion("effective_date_start not between", value1, value2, "effectiveDateStart");
            return (Criteria) this;
        }

        public Criteria andEffectiveDateEndIsNull() {
            addCriterion("effective_date_end is null");
            return (Criteria) this;
        }

        public Criteria andEffectiveDateEndIsNotNull() {
            addCriterion("effective_date_end is not null");
            return (Criteria) this;
        }

        public Criteria andEffectiveDateEndEqualTo(Date value) {
            addCriterion("effective_date_end =", value, "effectiveDateEnd");
            return (Criteria) this;
        }

        public Criteria andEffectiveDateEndNotEqualTo(Date value) {
            addCriterion("effective_date_end <>", value, "effectiveDateEnd");
            return (Criteria) this;
        }

        public Criteria andEffectiveDateEndGreaterThan(Date value) {
            addCriterion("effective_date_end >", value, "effectiveDateEnd");
            return (Criteria) this;
        }

        public Criteria andEffectiveDateEndGreaterThanOrEqualTo(Date value) {
            addCriterion("effective_date_end >=", value, "effectiveDateEnd");
            return (Criteria) this;
        }

        public Criteria andEffectiveDateEndLessThan(Date value) {
            addCriterion("effective_date_end <", value, "effectiveDateEnd");
            return (Criteria) this;
        }

        public Criteria andEffectiveDateEndLessThanOrEqualTo(Date value) {
            addCriterion("effective_date_end <=", value, "effectiveDateEnd");
            return (Criteria) this;
        }

        public Criteria andEffectiveDateEndIn(List<Date> values) {
            addCriterion("effective_date_end in", values, "effectiveDateEnd");
            return (Criteria) this;
        }

        public Criteria andEffectiveDateEndNotIn(List<Date> values) {
            addCriterion("effective_date_end not in", values, "effectiveDateEnd");
            return (Criteria) this;
        }

        public Criteria andEffectiveDateEndBetween(Date value1, Date value2) {
            addCriterion("effective_date_end between", value1, value2, "effectiveDateEnd");
            return (Criteria) this;
        }

        public Criteria andEffectiveDateEndNotBetween(Date value1, Date value2) {
            addCriterion("effective_date_end not between", value1, value2, "effectiveDateEnd");
            return (Criteria) this;
        }

        public Criteria andCouponNumberIsNull() {
            addCriterion("coupon_number is null");
            return (Criteria) this;
        }

        public Criteria andCouponNumberIsNotNull() {
            addCriterion("coupon_number is not null");
            return (Criteria) this;
        }

        public Criteria andCouponNumberEqualTo(Integer value) {
            addCriterion("coupon_number =", value, "couponNumber");
            return (Criteria) this;
        }

        public Criteria andCouponNumberNotEqualTo(Integer value) {
            addCriterion("coupon_number <>", value, "couponNumber");
            return (Criteria) this;
        }

        public Criteria andCouponNumberGreaterThan(Integer value) {
            addCriterion("coupon_number >", value, "couponNumber");
            return (Criteria) this;
        }

        public Criteria andCouponNumberGreaterThanOrEqualTo(Integer value) {
            addCriterion("coupon_number >=", value, "couponNumber");
            return (Criteria) this;
        }

        public Criteria andCouponNumberLessThan(Integer value) {
            addCriterion("coupon_number <", value, "couponNumber");
            return (Criteria) this;
        }

        public Criteria andCouponNumberLessThanOrEqualTo(Integer value) {
            addCriterion("coupon_number <=", value, "couponNumber");
            return (Criteria) this;
        }

        public Criteria andCouponNumberIn(List<Integer> values) {
            addCriterion("coupon_number in", values, "couponNumber");
            return (Criteria) this;
        }

        public Criteria andCouponNumberNotIn(List<Integer> values) {
            addCriterion("coupon_number not in", values, "couponNumber");
            return (Criteria) this;
        }

        public Criteria andCouponNumberBetween(Integer value1, Integer value2) {
            addCriterion("coupon_number between", value1, value2, "couponNumber");
            return (Criteria) this;
        }

        public Criteria andCouponNumberNotBetween(Integer value1, Integer value2) {
            addCriterion("coupon_number not between", value1, value2, "couponNumber");
            return (Criteria) this;
        }

        public Criteria andDpIsNull() {
            addCriterion("dp is null");
            return (Criteria) this;
        }

        public Criteria andDpIsNotNull() {
            addCriterion("dp is not null");
            return (Criteria) this;
        }

        public Criteria andDpEqualTo(String value) {
            addCriterion("dp =", value, "dp");
            return (Criteria) this;
        }

        public Criteria andDpNotEqualTo(String value) {
            addCriterion("dp <>", value, "dp");
            return (Criteria) this;
        }

        public Criteria andDpGreaterThan(String value) {
            addCriterion("dp >", value, "dp");
            return (Criteria) this;
        }

        public Criteria andDpGreaterThanOrEqualTo(String value) {
            addCriterion("dp >=", value, "dp");
            return (Criteria) this;
        }

        public Criteria andDpLessThan(String value) {
            addCriterion("dp <", value, "dp");
            return (Criteria) this;
        }

        public Criteria andDpLessThanOrEqualTo(String value) {
            addCriterion("dp <=", value, "dp");
            return (Criteria) this;
        }

        public Criteria andDpLike(String value) {
            addCriterion("dp like", value, "dp");
            return (Criteria) this;
        }

        public Criteria andDpNotLike(String value) {
            addCriterion("dp not like", value, "dp");
            return (Criteria) this;
        }

        public Criteria andDpIn(List<String> values) {
            addCriterion("dp in", values, "dp");
            return (Criteria) this;
        }

        public Criteria andDpNotIn(List<String> values) {
            addCriterion("dp not in", values, "dp");
            return (Criteria) this;
        }

        public Criteria andDpBetween(String value1, String value2) {
            addCriterion("dp between", value1, value2, "dp");
            return (Criteria) this;
        }

        public Criteria andDpNotBetween(String value1, String value2) {
            addCriterion("dp not between", value1, value2, "dp");
            return (Criteria) this;
        }

        public Criteria andMiniDpIsNull() {
            addCriterion("mini_dp is null");
            return (Criteria) this;
        }

        public Criteria andMiniDpIsNotNull() {
            addCriterion("mini_dp is not null");
            return (Criteria) this;
        }

        public Criteria andMiniDpEqualTo(String value) {
            addCriterion("mini_dp =", value, "miniDp");
            return (Criteria) this;
        }

        public Criteria andMiniDpNotEqualTo(String value) {
            addCriterion("mini_dp <>", value, "miniDp");
            return (Criteria) this;
        }

        public Criteria andMiniDpGreaterThan(String value) {
            addCriterion("mini_dp >", value, "miniDp");
            return (Criteria) this;
        }

        public Criteria andMiniDpGreaterThanOrEqualTo(String value) {
            addCriterion("mini_dp >=", value, "miniDp");
            return (Criteria) this;
        }

        public Criteria andMiniDpLessThan(String value) {
            addCriterion("mini_dp <", value, "miniDp");
            return (Criteria) this;
        }

        public Criteria andMiniDpLessThanOrEqualTo(String value) {
            addCriterion("mini_dp <=", value, "miniDp");
            return (Criteria) this;
        }

        public Criteria andMiniDpLike(String value) {
            addCriterion("mini_dp like", value, "miniDp");
            return (Criteria) this;
        }

        public Criteria andMiniDpNotLike(String value) {
            addCriterion("mini_dp not like", value, "miniDp");
            return (Criteria) this;
        }

        public Criteria andMiniDpIn(List<String> values) {
            addCriterion("mini_dp in", values, "miniDp");
            return (Criteria) this;
        }

        public Criteria andMiniDpNotIn(List<String> values) {
            addCriterion("mini_dp not in", values, "miniDp");
            return (Criteria) this;
        }

        public Criteria andMiniDpBetween(String value1, String value2) {
            addCriterion("mini_dp between", value1, value2, "miniDp");
            return (Criteria) this;
        }

        public Criteria andMiniDpNotBetween(String value1, String value2) {
            addCriterion("mini_dp not between", value1, value2, "miniDp");
            return (Criteria) this;
        }

        public Criteria andBvIsNull() {
            addCriterion("bv is null");
            return (Criteria) this;
        }

        public Criteria andBvIsNotNull() {
            addCriterion("bv is not null");
            return (Criteria) this;
        }

        public Criteria andBvEqualTo(String value) {
            addCriterion("bv =", value, "bv");
            return (Criteria) this;
        }

        public Criteria andBvNotEqualTo(String value) {
            addCriterion("bv <>", value, "bv");
            return (Criteria) this;
        }

        public Criteria andBvGreaterThan(String value) {
            addCriterion("bv >", value, "bv");
            return (Criteria) this;
        }

        public Criteria andBvGreaterThanOrEqualTo(String value) {
            addCriterion("bv >=", value, "bv");
            return (Criteria) this;
        }

        public Criteria andBvLessThan(String value) {
            addCriterion("bv <", value, "bv");
            return (Criteria) this;
        }

        public Criteria andBvLessThanOrEqualTo(String value) {
            addCriterion("bv <=", value, "bv");
            return (Criteria) this;
        }

        public Criteria andBvLike(String value) {
            addCriterion("bv like", value, "bv");
            return (Criteria) this;
        }

        public Criteria andBvNotLike(String value) {
            addCriterion("bv not like", value, "bv");
            return (Criteria) this;
        }

        public Criteria andBvIn(List<String> values) {
            addCriterion("bv in", values, "bv");
            return (Criteria) this;
        }

        public Criteria andBvNotIn(List<String> values) {
            addCriterion("bv not in", values, "bv");
            return (Criteria) this;
        }

        public Criteria andBvBetween(String value1, String value2) {
            addCriterion("bv between", value1, value2, "bv");
            return (Criteria) this;
        }

        public Criteria andBvNotBetween(String value1, String value2) {
            addCriterion("bv not between", value1, value2, "bv");
            return (Criteria) this;
        }

        public Criteria andMiniBvIsNull() {
            addCriterion("mini_bv is null");
            return (Criteria) this;
        }

        public Criteria andMiniBvIsNotNull() {
            addCriterion("mini_bv is not null");
            return (Criteria) this;
        }

        public Criteria andMiniBvEqualTo(String value) {
            addCriterion("mini_bv =", value, "miniBv");
            return (Criteria) this;
        }

        public Criteria andMiniBvNotEqualTo(String value) {
            addCriterion("mini_bv <>", value, "miniBv");
            return (Criteria) this;
        }

        public Criteria andMiniBvGreaterThan(String value) {
            addCriterion("mini_bv >", value, "miniBv");
            return (Criteria) this;
        }

        public Criteria andMiniBvGreaterThanOrEqualTo(String value) {
            addCriterion("mini_bv >=", value, "miniBv");
            return (Criteria) this;
        }

        public Criteria andMiniBvLessThan(String value) {
            addCriterion("mini_bv <", value, "miniBv");
            return (Criteria) this;
        }

        public Criteria andMiniBvLessThanOrEqualTo(String value) {
            addCriterion("mini_bv <=", value, "miniBv");
            return (Criteria) this;
        }

        public Criteria andMiniBvLike(String value) {
            addCriterion("mini_bv like", value, "miniBv");
            return (Criteria) this;
        }

        public Criteria andMiniBvNotLike(String value) {
            addCriterion("mini_bv not like", value, "miniBv");
            return (Criteria) this;
        }

        public Criteria andMiniBvIn(List<String> values) {
            addCriterion("mini_bv in", values, "miniBv");
            return (Criteria) this;
        }

        public Criteria andMiniBvNotIn(List<String> values) {
            addCriterion("mini_bv not in", values, "miniBv");
            return (Criteria) this;
        }

        public Criteria andMiniBvBetween(String value1, String value2) {
            addCriterion("mini_bv between", value1, value2, "miniBv");
            return (Criteria) this;
        }

        public Criteria andMiniBvNotBetween(String value1, String value2) {
            addCriterion("mini_bv not between", value1, value2, "miniBv");
            return (Criteria) this;
        }

        public Criteria andPvIsNull() {
            addCriterion("pv is null");
            return (Criteria) this;
        }

        public Criteria andPvIsNotNull() {
            addCriterion("pv is not null");
            return (Criteria) this;
        }

        public Criteria andPvEqualTo(String value) {
            addCriterion("pv =", value, "pv");
            return (Criteria) this;
        }

        public Criteria andPvNotEqualTo(String value) {
            addCriterion("pv <>", value, "pv");
            return (Criteria) this;
        }

        public Criteria andPvGreaterThan(String value) {
            addCriterion("pv >", value, "pv");
            return (Criteria) this;
        }

        public Criteria andPvGreaterThanOrEqualTo(String value) {
            addCriterion("pv >=", value, "pv");
            return (Criteria) this;
        }

        public Criteria andPvLessThan(String value) {
            addCriterion("pv <", value, "pv");
            return (Criteria) this;
        }

        public Criteria andPvLessThanOrEqualTo(String value) {
            addCriterion("pv <=", value, "pv");
            return (Criteria) this;
        }

        public Criteria andPvLike(String value) {
            addCriterion("pv like", value, "pv");
            return (Criteria) this;
        }

        public Criteria andPvNotLike(String value) {
            addCriterion("pv not like", value, "pv");
            return (Criteria) this;
        }

        public Criteria andPvIn(List<String> values) {
            addCriterion("pv in", values, "pv");
            return (Criteria) this;
        }

        public Criteria andPvNotIn(List<String> values) {
            addCriterion("pv not in", values, "pv");
            return (Criteria) this;
        }

        public Criteria andPvBetween(String value1, String value2) {
            addCriterion("pv between", value1, value2, "pv");
            return (Criteria) this;
        }

        public Criteria andPvNotBetween(String value1, String value2) {
            addCriterion("pv not between", value1, value2, "pv");
            return (Criteria) this;
        }

        public Criteria andSourceIsNull() {
            addCriterion("source is null");
            return (Criteria) this;
        }

        public Criteria andSourceIsNotNull() {
            addCriterion("source is not null");
            return (Criteria) this;
        }

        public Criteria andSourceEqualTo(String value) {
            addCriterion("source =", value, "source");
            return (Criteria) this;
        }

        public Criteria andSourceNotEqualTo(String value) {
            addCriterion("source <>", value, "source");
            return (Criteria) this;
        }

        public Criteria andSourceGreaterThan(String value) {
            addCriterion("source >", value, "source");
            return (Criteria) this;
        }

        public Criteria andSourceGreaterThanOrEqualTo(String value) {
            addCriterion("source >=", value, "source");
            return (Criteria) this;
        }

        public Criteria andSourceLessThan(String value) {
            addCriterion("source <", value, "source");
            return (Criteria) this;
        }

        public Criteria andSourceLessThanOrEqualTo(String value) {
            addCriterion("source <=", value, "source");
            return (Criteria) this;
        }

        public Criteria andSourceLike(String value) {
            addCriterion("source like", value, "source");
            return (Criteria) this;
        }

        public Criteria andSourceNotLike(String value) {
            addCriterion("source not like", value, "source");
            return (Criteria) this;
        }

        public Criteria andSourceIn(List<String> values) {
            addCriterion("source in", values, "source");
            return (Criteria) this;
        }

        public Criteria andSourceNotIn(List<String> values) {
            addCriterion("source not in", values, "source");
            return (Criteria) this;
        }

        public Criteria andSourceBetween(String value1, String value2) {
            addCriterion("source between", value1, value2, "source");
            return (Criteria) this;
        }

        public Criteria andSourceNotBetween(String value1, String value2) {
            addCriterion("source not between", value1, value2, "source");
            return (Criteria) this;
        }

        public Criteria andIsDeleteIsNull() {
            addCriterion("is_delete is null");
            return (Criteria) this;
        }

        public Criteria andIsDeleteIsNotNull() {
            addCriterion("is_delete is not null");
            return (Criteria) this;
        }

        public Criteria andIsDeleteEqualTo(Integer value) {
            addCriterion("is_delete =", value, "isDelete");
            return (Criteria) this;
        }

        public Criteria andIsDeleteNotEqualTo(Integer value) {
            addCriterion("is_delete <>", value, "isDelete");
            return (Criteria) this;
        }

        public Criteria andIsDeleteGreaterThan(Integer value) {
            addCriterion("is_delete >", value, "isDelete");
            return (Criteria) this;
        }

        public Criteria andIsDeleteGreaterThanOrEqualTo(Integer value) {
            addCriterion("is_delete >=", value, "isDelete");
            return (Criteria) this;
        }

        public Criteria andIsDeleteLessThan(Integer value) {
            addCriterion("is_delete <", value, "isDelete");
            return (Criteria) this;
        }

        public Criteria andIsDeleteLessThanOrEqualTo(Integer value) {
            addCriterion("is_delete <=", value, "isDelete");
            return (Criteria) this;
        }

        public Criteria andIsDeleteIn(List<Integer> values) {
            addCriterion("is_delete in", values, "isDelete");
            return (Criteria) this;
        }

        public Criteria andIsDeleteNotIn(List<Integer> values) {
            addCriterion("is_delete not in", values, "isDelete");
            return (Criteria) this;
        }

        public Criteria andIsDeleteBetween(Integer value1, Integer value2) {
            addCriterion("is_delete between", value1, value2, "isDelete");
            return (Criteria) this;
        }

        public Criteria andIsDeleteNotBetween(Integer value1, Integer value2) {
            addCriterion("is_delete not between", value1, value2, "isDelete");
            return (Criteria) this;
        }

        public Criteria andDistributionStatusIsNull() {
            addCriterion("distribution_status is null");
            return (Criteria) this;
        }

        public Criteria andDistributionStatusIsNotNull() {
            addCriterion("distribution_status is not null");
            return (Criteria) this;
        }

        public Criteria andDistributionStatusEqualTo(Integer value) {
            addCriterion("distribution_status =", value, "distributionStatus");
            return (Criteria) this;
        }

        public Criteria andDistributionStatusNotEqualTo(Integer value) {
            addCriterion("distribution_status <>", value, "distributionStatus");
            return (Criteria) this;
        }

        public Criteria andDistributionStatusGreaterThan(Integer value) {
            addCriterion("distribution_status >", value, "distributionStatus");
            return (Criteria) this;
        }

        public Criteria andDistributionStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("distribution_status >=", value, "distributionStatus");
            return (Criteria) this;
        }

        public Criteria andDistributionStatusLessThan(Integer value) {
            addCriterion("distribution_status <", value, "distributionStatus");
            return (Criteria) this;
        }

        public Criteria andDistributionStatusLessThanOrEqualTo(Integer value) {
            addCriterion("distribution_status <=", value, "distributionStatus");
            return (Criteria) this;
        }

        public Criteria andDistributionStatusIn(List<Integer> values) {
            addCriterion("distribution_status in", values, "distributionStatus");
            return (Criteria) this;
        }

        public Criteria andDistributionStatusNotIn(List<Integer> values) {
            addCriterion("distribution_status not in", values, "distributionStatus");
            return (Criteria) this;
        }

        public Criteria andDistributionStatusBetween(Integer value1, Integer value2) {
            addCriterion("distribution_status between", value1, value2, "distributionStatus");
            return (Criteria) this;
        }

        public Criteria andDistributionStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("distribution_status not between", value1, value2, "distributionStatus");
            return (Criteria) this;
        }

        public Criteria andAuditStatusIsNull() {
            addCriterion("audit_status is null");
            return (Criteria) this;
        }

        public Criteria andAuditStatusIsNotNull() {
            addCriterion("audit_status is not null");
            return (Criteria) this;
        }

        public Criteria andAuditStatusEqualTo(Integer value) {
            addCriterion("audit_status =", value, "auditStatus");
            return (Criteria) this;
        }

        public Criteria andAuditStatusNotEqualTo(Integer value) {
            addCriterion("audit_status <>", value, "auditStatus");
            return (Criteria) this;
        }

        public Criteria andAuditStatusGreaterThan(Integer value) {
            addCriterion("audit_status >", value, "auditStatus");
            return (Criteria) this;
        }

        public Criteria andAuditStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("audit_status >=", value, "auditStatus");
            return (Criteria) this;
        }

        public Criteria andAuditStatusLessThan(Integer value) {
            addCriterion("audit_status <", value, "auditStatus");
            return (Criteria) this;
        }

        public Criteria andAuditStatusLessThanOrEqualTo(Integer value) {
            addCriterion("audit_status <=", value, "auditStatus");
            return (Criteria) this;
        }

        public Criteria andAuditStatusIn(List<Integer> values) {
            addCriterion("audit_status in", values, "auditStatus");
            return (Criteria) this;
        }

        public Criteria andAuditStatusNotIn(List<Integer> values) {
            addCriterion("audit_status not in", values, "auditStatus");
            return (Criteria) this;
        }

        public Criteria andAuditStatusBetween(Integer value1, Integer value2) {
            addCriterion("audit_status between", value1, value2, "auditStatus");
            return (Criteria) this;
        }

        public Criteria andAuditStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("audit_status not between", value1, value2, "auditStatus");
            return (Criteria) this;
        }

        public Criteria andAuditMessageIsNull() {
            addCriterion("audit_message is null");
            return (Criteria) this;
        }

        public Criteria andAuditMessageIsNotNull() {
            addCriterion("audit_message is not null");
            return (Criteria) this;
        }

        public Criteria andAuditMessageEqualTo(String value) {
            addCriterion("audit_message =", value, "auditMessage");
            return (Criteria) this;
        }

        public Criteria andAuditMessageNotEqualTo(String value) {
            addCriterion("audit_message <>", value, "auditMessage");
            return (Criteria) this;
        }

        public Criteria andAuditMessageGreaterThan(String value) {
            addCriterion("audit_message >", value, "auditMessage");
            return (Criteria) this;
        }

        public Criteria andAuditMessageGreaterThanOrEqualTo(String value) {
            addCriterion("audit_message >=", value, "auditMessage");
            return (Criteria) this;
        }

        public Criteria andAuditMessageLessThan(String value) {
            addCriterion("audit_message <", value, "auditMessage");
            return (Criteria) this;
        }

        public Criteria andAuditMessageLessThanOrEqualTo(String value) {
            addCriterion("audit_message <=", value, "auditMessage");
            return (Criteria) this;
        }

        public Criteria andAuditMessageLike(String value) {
            addCriterion("audit_message like", value, "auditMessage");
            return (Criteria) this;
        }

        public Criteria andAuditMessageNotLike(String value) {
            addCriterion("audit_message not like", value, "auditMessage");
            return (Criteria) this;
        }

        public Criteria andAuditMessageIn(List<String> values) {
            addCriterion("audit_message in", values, "auditMessage");
            return (Criteria) this;
        }

        public Criteria andAuditMessageNotIn(List<String> values) {
            addCriterion("audit_message not in", values, "auditMessage");
            return (Criteria) this;
        }

        public Criteria andAuditMessageBetween(String value1, String value2) {
            addCriterion("audit_message between", value1, value2, "auditMessage");
            return (Criteria) this;
        }

        public Criteria andAuditMessageNotBetween(String value1, String value2) {
            addCriterion("audit_message not between", value1, value2, "auditMessage");
            return (Criteria) this;
        }

        public Criteria andAuditTimeIsNull() {
            addCriterion("audit_time is null");
            return (Criteria) this;
        }

        public Criteria andAuditTimeIsNotNull() {
            addCriterion("audit_time is not null");
            return (Criteria) this;
        }

        public Criteria andAuditTimeEqualTo(Date value) {
            addCriterion("audit_time =", value, "auditTime");
            return (Criteria) this;
        }

        public Criteria andAuditTimeNotEqualTo(Date value) {
            addCriterion("audit_time <>", value, "auditTime");
            return (Criteria) this;
        }

        public Criteria andAuditTimeGreaterThan(Date value) {
            addCriterion("audit_time >", value, "auditTime");
            return (Criteria) this;
        }

        public Criteria andAuditTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("audit_time >=", value, "auditTime");
            return (Criteria) this;
        }

        public Criteria andAuditTimeLessThan(Date value) {
            addCriterion("audit_time <", value, "auditTime");
            return (Criteria) this;
        }

        public Criteria andAuditTimeLessThanOrEqualTo(Date value) {
            addCriterion("audit_time <=", value, "auditTime");
            return (Criteria) this;
        }

        public Criteria andAuditTimeIn(List<Date> values) {
            addCriterion("audit_time in", values, "auditTime");
            return (Criteria) this;
        }

        public Criteria andAuditTimeNotIn(List<Date> values) {
            addCriterion("audit_time not in", values, "auditTime");
            return (Criteria) this;
        }

        public Criteria andAuditTimeBetween(Date value1, Date value2) {
            addCriterion("audit_time between", value1, value2, "auditTime");
            return (Criteria) this;
        }

        public Criteria andAuditTimeNotBetween(Date value1, Date value2) {
            addCriterion("audit_time not between", value1, value2, "auditTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("create_time is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("create_time is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("create_time =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("create_time <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("create_time >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("create_time >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("create_time <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("create_time <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("create_time in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("create_time not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("create_time between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("create_time not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateUserIsNull() {
            addCriterion("create_user is null");
            return (Criteria) this;
        }

        public Criteria andCreateUserIsNotNull() {
            addCriterion("create_user is not null");
            return (Criteria) this;
        }

        public Criteria andCreateUserEqualTo(String value) {
            addCriterion("create_user =", value, "createUser");
            return (Criteria) this;
        }

        public Criteria andCreateUserNotEqualTo(String value) {
            addCriterion("create_user <>", value, "createUser");
            return (Criteria) this;
        }

        public Criteria andCreateUserGreaterThan(String value) {
            addCriterion("create_user >", value, "createUser");
            return (Criteria) this;
        }

        public Criteria andCreateUserGreaterThanOrEqualTo(String value) {
            addCriterion("create_user >=", value, "createUser");
            return (Criteria) this;
        }

        public Criteria andCreateUserLessThan(String value) {
            addCriterion("create_user <", value, "createUser");
            return (Criteria) this;
        }

        public Criteria andCreateUserLessThanOrEqualTo(String value) {
            addCriterion("create_user <=", value, "createUser");
            return (Criteria) this;
        }

        public Criteria andCreateUserLike(String value) {
            addCriterion("create_user like", value, "createUser");
            return (Criteria) this;
        }

        public Criteria andCreateUserNotLike(String value) {
            addCriterion("create_user not like", value, "createUser");
            return (Criteria) this;
        }

        public Criteria andCreateUserIn(List<String> values) {
            addCriterion("create_user in", values, "createUser");
            return (Criteria) this;
        }

        public Criteria andCreateUserNotIn(List<String> values) {
            addCriterion("create_user not in", values, "createUser");
            return (Criteria) this;
        }

        public Criteria andCreateUserBetween(String value1, String value2) {
            addCriterion("create_user between", value1, value2, "createUser");
            return (Criteria) this;
        }

        public Criteria andCreateUserNotBetween(String value1, String value2) {
            addCriterion("create_user not between", value1, value2, "createUser");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNull() {
            addCriterion("update_time is null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNotNull() {
            addCriterion("update_time is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeEqualTo(Date value) {
            addCriterion("update_time =", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotEqualTo(Date value) {
            addCriterion("update_time <>", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThan(Date value) {
            addCriterion("update_time >", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("update_time >=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThan(Date value) {
            addCriterion("update_time <", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThanOrEqualTo(Date value) {
            addCriterion("update_time <=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIn(List<Date> values) {
            addCriterion("update_time in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotIn(List<Date> values) {
            addCriterion("update_time not in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeBetween(Date value1, Date value2) {
            addCriterion("update_time between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotBetween(Date value1, Date value2) {
            addCriterion("update_time not between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateUserIsNull() {
            addCriterion("update_user is null");
            return (Criteria) this;
        }

        public Criteria andUpdateUserIsNotNull() {
            addCriterion("update_user is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateUserEqualTo(String value) {
            addCriterion("update_user =", value, "updateUser");
            return (Criteria) this;
        }

        public Criteria andUpdateUserNotEqualTo(String value) {
            addCriterion("update_user <>", value, "updateUser");
            return (Criteria) this;
        }

        public Criteria andUpdateUserGreaterThan(String value) {
            addCriterion("update_user >", value, "updateUser");
            return (Criteria) this;
        }

        public Criteria andUpdateUserGreaterThanOrEqualTo(String value) {
            addCriterion("update_user >=", value, "updateUser");
            return (Criteria) this;
        }

        public Criteria andUpdateUserLessThan(String value) {
            addCriterion("update_user <", value, "updateUser");
            return (Criteria) this;
        }

        public Criteria andUpdateUserLessThanOrEqualTo(String value) {
            addCriterion("update_user <=", value, "updateUser");
            return (Criteria) this;
        }

        public Criteria andUpdateUserLike(String value) {
            addCriterion("update_user like", value, "updateUser");
            return (Criteria) this;
        }

        public Criteria andUpdateUserNotLike(String value) {
            addCriterion("update_user not like", value, "updateUser");
            return (Criteria) this;
        }

        public Criteria andUpdateUserIn(List<String> values) {
            addCriterion("update_user in", values, "updateUser");
            return (Criteria) this;
        }

        public Criteria andUpdateUserNotIn(List<String> values) {
            addCriterion("update_user not in", values, "updateUser");
            return (Criteria) this;
        }

        public Criteria andUpdateUserBetween(String value1, String value2) {
            addCriterion("update_user between", value1, value2, "updateUser");
            return (Criteria) this;
        }

        public Criteria andUpdateUserNotBetween(String value1, String value2) {
            addCriterion("update_user not between", value1, value2, "updateUser");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}