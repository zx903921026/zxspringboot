package com.zx.springbootmybatis.model;

import lombok.Data;

import java.util.UUID;
@Data
public class ResultResponse<T> {

    private static final String SUCCESS_MSG = "success";
    private String requestId;
    private String code;
    private String message;
    private Boolean success;
    private T Data;
    public ResultResponse() {
        this.requestId = UUID.randomUUID().toString();
    }

    public ResultResponse(String reqId) {
        this.requestId = reqId;
    }

    public ResultResponse(String reqId, T payload) {
        this.requestId = reqId;
        this.code = "";
        this.Data = payload;
        this.success = true;
    }

    public ResultResponse(String reqId, String code, String msg) {
        this.requestId = reqId;
        this.code = code;
        this.message = msg;
        this.success = true;
    }

    public ResultResponse(String reqId, String code, String msg, T data) {
        this.requestId = reqId;
        this.code = code;
        this.message = msg;
        this.Data = data;
        this.success = true;
    }

    public static <T> ResultResponse<T> succResult() {
        ResultResponse<T> resultResponse = new ResultResponse();
        resultResponse.setCode("0");
        resultResponse.setSuccess(true);
        resultResponse.setMessage("success");
        return resultResponse;
    }

    public static <T> ResultResponse<T> succResult(T data) {
        ResultResponse<T> resultDTO = new ResultResponse();
        resultDTO.setCode("0");
        resultDTO.setSuccess(true);
        resultDTO.setMessage("success");
        resultDTO.setData(data);
        return resultDTO;
    }

    public static <T> ResultResponse<T> succResult(String reqId, T data) {
        ResultResponse<T> resultResponse = new ResultResponse();
        resultResponse.setRequestId(reqId);
        resultResponse.setCode("0");
        resultResponse.setSuccess(true);
        resultResponse.setMessage("success");
        resultResponse.setData(data);
        return resultResponse;
    }

    public static <T> ResultResponse<T> errorResult(String message) {
        return errorResult("500", message);
    }

    public static <T> ResultResponse<T> errorResult(String code, String message) {
        ResultResponse<T> resultDTO = new ResultResponse();
        resultDTO.setCode(code);
        resultDTO.setMessage(message);
        resultDTO.setSuccess(false);
        return resultDTO;
    }

    public static <T> ResultResponse<T> errorResult(String reqId, String code, String message) {
        ResultResponse<T> resultResponse = new ResultResponse();
        resultResponse.setRequestId(reqId);
        resultResponse.setCode(code);
        resultResponse.setMessage(message);
        resultResponse.setSuccess(false);
        return resultResponse;
    }

    public static <T> ResultResponse<T> errorResult(String reqId, String code, String message, T data) {
        ResultResponse<T> resultResponse = new ResultResponse();
        resultResponse.setRequestId(reqId);
        resultResponse.setCode(code);
        resultResponse.setMessage(message);
        resultResponse.setSuccess(false);
        resultResponse.setData(data);
        return resultResponse;
    }

    public static <T> ResultResponse<T> notImplement(T dummy) {
        ResultResponse<T> resultDTO = new ResultResponse();
        resultDTO.setCode("0");
        resultDTO.setMessage("暂时还没有实现这个API哟");
        resultDTO.setSuccess(true);
        resultDTO.setData(dummy);
        return resultDTO;
    }



}
