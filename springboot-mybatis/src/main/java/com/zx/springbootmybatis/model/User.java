package com.zx.springbootmybatis.model;

import lombok.Data;

@Data
public class User {

    String name;
    String password;
}
