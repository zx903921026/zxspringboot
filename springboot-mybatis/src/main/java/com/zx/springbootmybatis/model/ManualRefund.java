package com.zx.springbootmybatis.model;

import java.util.Date;

public class ManualRefund {
    private Integer id;

    private String ggNumber;

    private String ooNumber;

    private String tradeDist;

    private String company;

    private String refundAmount;

    private String message;

    private Integer refundStatus;

    private Date createTime;

    private Date updateTime;

    private String createUser;

    private String createUserId;

    private Integer isDelete;

    private Integer auditId;

    private String auditErrorMessage;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getGgNumber() {
        return ggNumber;
    }

    public void setGgNumber(String ggNumber) {
        this.ggNumber = ggNumber;
    }

    public String getOoNumber() {
        return ooNumber;
    }

    public void setOoNumber(String ooNumber) {
        this.ooNumber = ooNumber;
    }

    public String getTradeDist() {
        return tradeDist;
    }

    public void setTradeDist(String tradeDist) {
        this.tradeDist = tradeDist;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getRefundAmount() {
        return refundAmount;
    }

    public void setRefundAmount(String refundAmount) {
        this.refundAmount = refundAmount;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getRefundStatus() {
        return refundStatus;
    }

    public void setRefundStatus(Integer refundStatus) {
        this.refundStatus = refundStatus;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    public Integer getAuditId() {
        return auditId;
    }

    public void setAuditId(Integer auditId) {
        this.auditId = auditId;
    }

    public String getAuditErrorMessage() {
        return auditErrorMessage;
    }

    public void setAuditErrorMessage(String auditErrorMessage) {
        this.auditErrorMessage = auditErrorMessage;
    }
}