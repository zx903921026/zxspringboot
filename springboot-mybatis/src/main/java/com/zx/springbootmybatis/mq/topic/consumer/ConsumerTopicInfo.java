package com.zx.springbootmybatis.mq.topic.consumer;

import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.*;
import org.springframework.stereotype.Component;

/**
 * @QueueBinding value 绑定队列的名称
 * autodelete是否可删除的零时队列
 * key ：路由器
 */
@Component
@RabbitListener(bindings = @QueueBinding
        (value = @Queue(value = "${mq.config.queue.info}",autoDelete = "true")
        ,exchange = @Exchange(value = "${mq.config.topicExchange}",
        type = ExchangeTypes.TOPIC),
        key = "*.log.info")
)
public class ConsumerTopicInfo {


    @RabbitHandler
    public  void process(String msg){
        System.out.println("消费者消费topicInfo："+msg);
    }
}
