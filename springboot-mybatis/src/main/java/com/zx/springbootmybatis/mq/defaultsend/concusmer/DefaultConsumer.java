package com.zx.springbootmybatis.mq.defaultsend.concusmer;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
public class DefaultConsumer {

    @RabbitListener(queues = {"${mq.queue.name}"})
    public void process(String msg){
        System.out.println("消费者消费消息 :"+msg);
    }
}
