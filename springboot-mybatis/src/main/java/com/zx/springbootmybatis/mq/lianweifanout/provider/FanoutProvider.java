package com.zx.springbootmybatis.mq.lianweifanout.provider;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * 消息提供者
 */
@Component
public class FanoutProvider {
    @Autowired
    AmqpTemplate template;

    @Value("${mq.config.fanout.exchange}")
    String exchange;



    public void send(String msg){
        template.convertAndSend("xch-fanout-webm-cc-product-label","" ,msg);
    }


}
