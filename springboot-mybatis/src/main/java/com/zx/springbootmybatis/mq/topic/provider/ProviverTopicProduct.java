package com.zx.springbootmybatis.mq.topic.provider;


import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ProviverTopicProduct {

    @Autowired
    AmqpTemplate template;

    @Value("${mq.config.topicExchange}")
    String exchange;

    public void send(String msg){
        template.convertAndSend(exchange, "Product.log.debug", "Product.log.debug"+ msg);
        template.convertAndSend(exchange, "Product.log.info", "Product.log.info"+ msg);
        template.convertAndSend(exchange, "Product.log.warn", "Product.log.warn"+ msg);
        template.convertAndSend(exchange, "Product.log.error", "Product.log.error"+ msg);
    }


}
