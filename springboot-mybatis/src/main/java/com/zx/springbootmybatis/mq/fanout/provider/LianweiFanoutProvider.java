package com.zx.springbootmybatis.mq.fanout.provider;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * 消息提供者
 */
@Component
public class LianweiFanoutProvider {
    @Autowired
    AmqpTemplate template;

    @Value("${mq.config.fanout.exchange}")
    String exchange;



    public void send(String msg){
        template.convertAndSend("xch-fanout-webm-cc-product-label","que-webm-cc-product-label-001" ,msg);
    }


}
