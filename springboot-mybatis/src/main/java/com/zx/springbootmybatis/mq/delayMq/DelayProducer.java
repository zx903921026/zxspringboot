package com.zx.springbootmybatis.mq.delayMq;

import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
@Component
public class DelayProducer {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Resource(name = "proExchange")
    private TopicExchange proExchange;

    public void sendEmail() {
        String message = "1";
        String routingKey = "email";
        rabbitTemplate.convertAndSend(proExchange.getName(), routingKey, message);
        System.out.println("send successfully");
    }
}
