package com.zx.springbootmybatis.mq.delayMq;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
@Component
public class DelayConsumer {

    @Autowired
    private FanoutExchange conExchange;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @RabbitListener(queues = {"proQueue"})
    public void receiveEmail(int email, Channel channel, Message message) throws IOException {
        System.out.println(email);
        int num=Integer.valueOf(email);
        if(num==3){
            channel.basicAck(message.getMessageProperties().getDeliveryTag(),true);
            System.out.println("执行成功");
        }else{
            String routingKey = "email";
            MessageProperties messageProperties = getMessageProperties();
            num+=1;
            System.out.println("继续等待"+num);
            rabbitTemplate.convertAndSend(conExchange.getName(), routingKey, new Message((String.valueOf(num)).getBytes("utf-8"),messageProperties));
        }

     }

    /**
     * 设置消息的相关参数
     * @return
     */
    private MessageProperties getMessageProperties() {
        MessageProperties messageProperties = new MessageProperties();
        //设置消息的有效期 ms为单位
        messageProperties.setExpiration("5000");
        messageProperties.setContentType(MessageProperties.CONTENT_TYPE_TEXT_PLAIN);
        return messageProperties;
    }
}
