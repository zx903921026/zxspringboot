package com.zx.springbootmybatis.mq.direct.provider;


import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ProviverDirect {

    @Autowired
    AmqpTemplate template;

    @Value("${mq.config.exchange}")
    String exchange;

    @Value("${mq.queue.rounting.key.error}")
    String routingkey;

    public void send(String msg){
        template.convertAndSend(exchange,routingkey,msg);
    }


}
