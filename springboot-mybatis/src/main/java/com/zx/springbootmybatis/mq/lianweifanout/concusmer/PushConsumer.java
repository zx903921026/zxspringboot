package com.zx.springbootmybatis.mq.lianweifanout.concusmer;

import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.*;
import org.springframework.stereotype.Component;

@Component
@RabbitListener(bindings = @QueueBinding
        (value = @Queue(value = "${mq.config.fanout.queue.push}",autoDelete = "true")
                ,exchange = @Exchange(value = "${mq.config.fanout.exchange}",
                type = ExchangeTypes.FANOUT))
)
public class PushConsumer {

    @RabbitHandler
    public void process(String msg){
        System.out.println("PushConsumer消费消息 :"+msg);
    }
}
