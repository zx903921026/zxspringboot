package com.zx.springbootmybatis.mq.topic.provider;


import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ProviverTopicUser {

    @Autowired
    AmqpTemplate template;

    @Value("${mq.config.topicExchange}")
    String exchange;

    public void send(String msg){
        template.convertAndSend(exchange, "user.log.debug", "user.log.debug"+ msg);
        template.convertAndSend(exchange, "user.log.info", "user.log.info"+ msg);
        template.convertAndSend(exchange, "user.log.warn", "user.log.warn"+ msg);
        template.convertAndSend(exchange, "user.log.error", "user.log.error"+ msg);
    }


}
