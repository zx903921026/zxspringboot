package com.zx.springbootmybatis.mq.defaultsend.provider;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * 消息提供者
 */
@Component
public class DefaultProvider {
    @Autowired
    private AmqpTemplate template;
    @Value("${mq.queue.name}")
    private String queueName;

    /**
     * 发送消息
     * @param msg
     */
    public void send(String msg){
        template.convertAndSend(queueName,msg);
    }

}
