package com.zx.springbootmybatis.mq.direct.consumer;

import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.*;
import org.springframework.stereotype.Component;

/**
 * @QueueBinding value 绑定队列的名称
 * autodelete是否可删除的零时队列
 * key ：路由器
 */
@Component
@RabbitListener(bindings = @QueueBinding
        (value = @Queue(value = "${mq.queue.name.error}",autoDelete = "true")
        ,exchange = @Exchange(value = "${mq.config.exchange}",
        type = ExchangeTypes.DIRECT),
        key = {"${mq.queue.rounting.key.info}"})
)
public class ConsumerInfoDirect {


    @RabbitHandler
    public  void process(String msg){
        System.out.println("消费者消费Directinfo："+msg);
    }
}
