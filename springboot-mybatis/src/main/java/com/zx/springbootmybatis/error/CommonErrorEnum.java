package com.zx.springbootmybatis.error;

import com.zx.springbootmybatis.exception.IResultCode;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum CommonErrorEnum implements IResultCode {

    PARAM_VALIDATE_ERROR("CC.P.PARAM_VALIDATE_ERROR.01","Parameter verification exception"),
    UNKNOWN_EXCEPTION("CC.S.UNKNOWN_EXCEPTION.01","Unknown exception");

    private String errorCode;

    private String errorMessage;



}
