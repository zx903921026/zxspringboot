package com.zx.springbootmybatis.exception;


import com.alibaba.fastjson.JSONObject;
import com.zx.springbootmybatis.util.FileErrorEnum;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * Created by Kalend Zhang on 2019/7/18.
 *
 * @author zx
 * @date 2019/7/18 14:35
 */
@Slf4j
@Data
public class CommonException extends RuntimeException {


    private IResultCode resultCode;
    /**
     * 错误码
     */
    private String errorCode;

    /**
     * 错误消息
     */
    private String errorMessage;
    private String data;
    public CommonException(String message) {
        super(message);
    }

    public CommonException(IResultCode resultCode, String data) {
        super(resultCode.getErrorMessage());
        this.resultCode = resultCode;
        this.data = data;
    }
    public CommonException(IResultCode resultCode, Throwable cause) {
        super(cause);
        this.resultCode = resultCode;
    }
    public CommonException(IResultCode resultCode) {
        super(JSONObject.toJSONString(resultCode));
        this.resultCode = resultCode;
        this.errorCode = resultCode.getErrorCode();
        this.errorMessage = resultCode.getErrorMessage();
    }

    public CommonException(FileErrorEnum errorImportFile) {

    }

    @Override
    public Throwable fillInStackTrace() {
        return this;
    }

}
