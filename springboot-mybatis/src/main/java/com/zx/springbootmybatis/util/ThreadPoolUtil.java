package com.zx.springbootmybatis.util;

import java.util.concurrent.*;

public class ThreadPoolUtil {
    public ExecutorService GetThreadPool() {
        int nThreads = Runtime.getRuntime().availableProcessors();
        ExecutorService executorService = new ThreadPoolExecutor(nThreads, nThreads,
                0L, TimeUnit.MILLISECONDS,
                new LinkedBlockingQueue<>(),
                (runnable) -> {
                    Thread t = new Thread(runnable);
                    t.setName("productSetInactiveThread");
                    return t;
                });
        return executorService;
    }


    public static void main(String[] args) {
//        Future result = executorService.submit(()->{
//            String checkResult = checkPrice(skuDO);
//            return checkResult;
//        });
    }
}
