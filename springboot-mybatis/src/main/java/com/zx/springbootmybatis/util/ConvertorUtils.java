package com.zx.springbootmybatis.util;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import ma.glasnost.orika.metadata.Type;
import ma.glasnost.orika.metadata.TypeFactory;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 复制属性类
 *
 * @author James.Luo
 */
@Slf4j
@Component
@AllArgsConstructor
public class ConvertorUtils {

    private static MapperFacade mapper;
    private static MapperFactory mapperFactory;

    static {
        mapperFactory = new DefaultMapperFactory.Builder().build();
        mapper = mapperFactory.getMapperFacade();
    }

    public static MapperFactory getMapperFactory(){
        return mapperFactory;
    }

    public static <S, D> D copyProperties(S source, Class<D> destination) {
        return mapper.map(source, destination);
    }

    /**
     * 极致性能的复制出新类型对象.
     * 预先通过ObjectUtils.getType() 静态获取并缓存Type类型，在此处传入
     */
    public static <S, D> D copyProperties(S source, Type<S> sourceType, Type<D> destinationType) {
        return mapper.map(source, sourceType, destinationType);
    }

    /*public static <S, D> D copyPropertiesCusomized(S source, Class<D> targetType, MappingContext mappingContext) {
        return mapper.map(source, targetType, mappingContext);
    }*/

    /**
     * 极致性能的复制出新类型对象到Array.
     * 预先通过ObjectUtils.getType() 静态获取并缓存Type类型，在此处传入
     * @return
     */
    public static <S, D> D[] copyArray(S[] sourceArr, Type<S> sourceType, D[] destinationArr, Type<D> destinationType) {
        return mapper.mapAsArray(destinationArr, sourceArr, sourceType, destinationType);
    }

    /**
     * 极致性能的复制出新类型对象到ArrayList.
     * 预先通过ObjectUtils.getType() 静态获取并缓存Type类型，在此处传入
     */
    public static <S, D> List<D> copyList(Iterable<S> sourceList, Type<S> sourceType, Type<D> destinationType) {
        return mapper.mapAsList(sourceList, sourceType, destinationType);
    }

    public static <S, D> List<D> copyList(Iterable<S> sourceList, Type<S> sourceType, Type<D> destinationType,MappingContext mappingContext) {
        return mapper.mapAsList(sourceList, sourceType, destinationType,mappingContext);
    }

    /**
     * 预先获取orika转换所需要的Type，避免每次转换.
     */
    public static <E> Type<E> getType(final Class<E> rawType) {
        return TypeFactory.valueOf(rawType);
    }

    public static <S, D> D mapAsObj( S source, Class<D> destination){
        return mapper.map(source, destination);
     }

    public static <S, D> List<D> mapAsList( List<S> list, Class<D> destination){
        return mapper.mapAsList(list, destination);
    }

    public  static <S, D> List<D> mapAsList(Iterable<S> source, Class<D> destinationClass, MappingContext context) {
        return mapper.mapAsList(source, TypeFactory.elementTypeOf(source), TypeFactory.valueOf(destinationClass), context);
    }
}
