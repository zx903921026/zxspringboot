package com.zx.springbootmybatis.util;


import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @Author: ly
 * @Title: LandingErrorEnum
 * @Description
 * @Date: 2020/11/18 15:15
 */
@Getter
@AllArgsConstructor
public enum FileErrorEnum implements IResultCode {
    ERROR_IMPORT_FILE_EMPTY("1006101","导入文件失败，未获取到文件信息"),
    ERROR_IMPORT_TYPE("1006102","活动模板不支持，请重新选择菜单"),
    ERROR_IMPORT_FILE("1006103","文件导入失败，程序异常"),
    ERROR_IMPORT_ACTIVITY_EXIST("1006104","文件导入失败，活动已存在！"),
    ERROR_ZIP_FILES("1006105","文件压缩失败，程序异常"),
    ERROR_EXPORT_FILE("1006106","文件导出失败，程序异常"),
    ERROR_IMPORT_FILE_TYPE("1006107","文件导入失败，文件格式不正确，请选择zip格式文件"),
    ERROR_NO_OSS("1006108","处理文件失败，所选活动中文件类型错误"),
    ERROR_IMPORT_NOTICE_EXIST("1006111","文件导入失败，强通知消息已存在！"),
    ERROR_IMPORT_LIVE("1006109","导入文件非直播配置，请重新选择"),
    ERROR_IMPORT_LIVE_EXIST("1006110","文件导入失败，直播配置已存在！"),
    ERROR_IMPORT_DATA_ERROR("1006111","文件导入失败，导入数据缺失，请检查导入文件"),
    ERROR_IMPORT_DATA_EXIST("1006112","文件导入失败，导入信息已存在！"),
    ERROR_IMPORT_NEW_ACTIVITY_EXIST("1006113","文件导入失败，新活动已存在！"),
    ERROR_IMPORT_NAVIGATION("1006114","文件导入失败，活动格式有误！"),
    ERROR_IMPORT_CAROUSEL("1006115","导入文件非轮播图配置，请重新选择"),
    ERROR_URL_DECODE("1006116","处理失败，地址解码失败"),
    ERROR_IMPORT_EDITION("1006117","导入文件非版本记录配置，请重新选择"),
    ERROR_IMPORT_EDITION_EXIST("1006118","文件导入失败，版本记录配置已存在！"),
    ERROR_IMPORT_QA_QUESTION("1006119", "导入文件格式不符合要求，请检查后重新导入文件"),
    ERROR_CREATE_FORECAST_EXIST("1006120","保存失败，关联的活动Landing页已经创建预告页！"),
    ERROR_IMPORT_FORECAST_EXIST("1006121","导入失败，关联的活动Landing页已经创建预告页！"),
    ERROR_IMPORT_LANDING_NOT_EXIST("1006122","导入失败，关联的活动Landing页不存在！"),
    ERROR_IMPORT_COUPON_EXIST("1006123","电子券券码已存在！"),
    ERROR_IMPORT_TERMS("1006124","导入文件非服务条款配置，请重新选择"),
    ERROR_IMPORT_SKU_EXIST("1006125","SKU编码已存在！"),
    ;

    /**
     * 错误码
     */
    private String errorCode;
    /**
     * 错误信息
     */
    private String errorMessage;
}
