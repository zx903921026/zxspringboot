package com.zx.springbootmybatis.util;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.zx.springbootmybatis.dto.ExcelDTO;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.io.InputStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Component
public class ExcelUtil {
    public List<ExcelDTO> getExcelData(InputStream inputStream, String fileName) throws Exception {
        List<ExcelDTO> list = new ArrayList<>();

        boolean isE2007 = false;
        //判断是否是excel2007格式
        if(fileName.endsWith("xlsx")){
            isE2007 = true;
        }
        int rowIndex = 0;

        InputStream input = inputStream;  //建立输入流
        Workbook wb;
        //根据文件格式(2003或者2007)来初始化
        if(isE2007){
            wb = new XSSFWorkbook(input);
        }else{
            wb = new HSSFWorkbook(input);
        }
        Sheet sheet = wb.getSheetAt(0);    //获得第一个表单
        int rowCount = sheet.getLastRowNum()+1;
        Row row = sheet.getRow(0);

        //总列数
        int colLength = row.getLastCellNum();

        for(int i = 1; i < rowCount;i++){
            ExcelDTO importListDTO = new ExcelDTO();
            try {
                rowIndex = i;
                row = sheet.getRow(i);
                for(int j = 0;j<colLength;j++){
                    String cellValue = null;
                    if(isMergedRegion(sheet,i,j)){
                        cellValue = getMergedRegionValue(sheet,i,j);
                    }else{
                        row = sheet.getRow(i);
                        Cell cell = row.getCell(j);
                        if (cell != null) {
                            cell.setCellType(Cell.CELL_TYPE_STRING);
                            cellValue = cell.getStringCellValue();
                        }
                    }
                    cellValue = cellValue.trim();
                    DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                    switch (j) {
                        case 0:
                            importListDTO.setColumn0(cellValue);
                            break;
                        case 1:
                            importListDTO.setColumn1(cellValue);
                            break;
                        case 2:
                            importListDTO.setColumn2(cellValue);
                            break;
                        case 3:
                            importListDTO.setColumn3(cellValue);
                            break;
                        case 4:
                            importListDTO.setColumn4(cellValue);
                            break;
                    }

                }
            }catch (Exception e){
//                importListDTO.getErrormeaasges().add(e.getMessage());
            }
//            if(importListDTO.getErrormeaasges().size()>0){
//                importListDTO.setErrorFlag(1);
//            }
            list.add(importListDTO);
        }
        return list;
    }
    /**
     * 判断指定的单元格是否是合并单元格
     * @param sheet
     * @param row 行下标
     * @param column 列下标
     * @return
     */
    private  boolean isMergedRegion(Sheet sheet,int row ,int column) {
        int sheetMergeCount = sheet.getNumMergedRegions();
        for (int i = 0; i < sheetMergeCount; i++) {
            CellRangeAddress range = sheet.getMergedRegion(i);
            int firstColumn = range.getFirstColumn();
            int lastColumn = range.getLastColumn();
            int firstRow = range.getFirstRow();
            int lastRow = range.getLastRow();
            if(row >= firstRow && row <= lastRow){
                if(column >= firstColumn && column <= lastColumn){
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * 获取合并单元格的值
     * @param sheet
     * @param row
     * @param column
     * @return
     */
    public  String getMergedRegionValue(Sheet sheet ,int row , int column){
        int sheetMergeCount = sheet.getNumMergedRegions();

        for(int i = 0 ; i < sheetMergeCount ; i++){
            CellRangeAddress ca = sheet.getMergedRegion(i);
            int firstColumn = ca.getFirstColumn();
            int lastColumn = ca.getLastColumn();
            int firstRow = ca.getFirstRow();
            int lastRow = ca.getLastRow();

            if(row >= firstRow && row <= lastRow){
                if(column >= firstColumn && column <= lastColumn){
                    Row fRow = sheet.getRow(firstRow);
                    Cell fCell = fRow.getCell(firstColumn);
                    //获取单元格的值
                    if (fCell == null) {
                        return "";
                    }
                    return fCell.getStringCellValue();
                }
            }
        }

        return null ;
    }


}
