package com.zx.springbootmybatis.util;

import com.alibaba.fastjson.JSONObject;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.zx.springbootmybatis.exception.CommonException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

/**
 * @Author: ly
 * @Title: FileUtil
 * @Description
 * @Date: 2020/11/17 15:31
 */
@Slf4j
public class FileUtil {
    private static final int  BUFFER_SIZE = 8 * 1024;

    public static String readFile(File importFile){
        InputStream importInput = null;
        try {
            importInput = new FileInputStream(importFile);
            //1024*8
            byte[] bytes = new byte[8192];
            int length;
            StringBuilder stringBuilder = new StringBuilder();
            InputStreamReader isr = new InputStreamReader(importInput,"UTF-8");
            BufferedReader br = new BufferedReader(isr);
            String line = null;
            while ((line = br.readLine()) != null) {
                stringBuilder.append(line + "\n");
            }
//            while ( (length = importInput.read(bytes)) != -1 ){
//                stringBuilder.append(new String(bytes,0,length,"UTF-8"));
//            }
            if( StringUtils.isBlank(stringBuilder.toString()) ){
                throw new CommonException(FileErrorEnum.ERROR_IMPORT_FILE_EMPTY);
            }
            return stringBuilder.toString();
        }catch (Exception e){
            throw new CommonException(FileErrorEnum.ERROR_IMPORT_FILE_EMPTY);
        }finally {
            try {
                if( null != importInput ){
                    importInput.close();
                }
            } catch (IOException io){
                log.error("读取文件失败，异常原因：{}", io.getMessage());
                throw new CommonException(FileErrorEnum.ERROR_IMPORT_FILE);
            }
        }
    }


    public static String readFileByReader(MultipartFile importFile){
        InputStreamReader importInput = null;
        try {
            importInput = new InputStreamReader(importFile.getInputStream());
            //1024*8
            char[] bytes = new char[8192];
            int length;
            StringBuilder stringBuilder = new StringBuilder();
            while ( (length = importInput.read(bytes)) != -1 ){
                stringBuilder.append(new String(bytes,0,length));
            }
            if( StringUtils.isBlank(stringBuilder.toString()) ){
                throw new CommonException(FileErrorEnum.ERROR_IMPORT_FILE_EMPTY);
            }
            return stringBuilder.toString();
        }catch (Exception e){
            throw new CommonException(FileErrorEnum.ERROR_IMPORT_FILE_EMPTY);
        }finally {
            try {
                if( null != importInput ){
                    importInput.close();
                }
            } catch (IOException io){
                throw new CommonException(FileErrorEnum.ERROR_IMPORT_FILE);
            }
        }
    }

    public static void toZip(String srcDir, OutputStream out, boolean KeepDirStructure){
        ZipOutputStream zipOutputStream = null;
        try {
            zipOutputStream = new ZipOutputStream(out);
            File sourceFile = new File(srcDir);
            compress(sourceFile,zipOutputStream,sourceFile.getName(),KeepDirStructure);
        }catch (Exception e){
            log.error("压缩文件失败，失败原因：{}", JSONObject.toJSONString(e));
            throw new CommonException(FileErrorEnum.ERROR_ZIP_FILES);
        }finally {
            if( null != zipOutputStream ){
                try {
                    zipOutputStream.close();
                }catch (Exception e){
                    log.error("压缩文件失败，失败原因：{}", JSONObject.toJSONString(e));
                    throw new CommonException(FileErrorEnum.ERROR_ZIP_FILES);
                }
            }
        }
    }

    public static void deleteFile(String fileSrc){
        deleteFile(new File(fileSrc));
    }

    public static void deleteFile(File file){
        if( !file.exists() ){
            return;
        }
        if( file.isDirectory() ){
            //delete
            File[] listFiles = file.listFiles();
            for(File listFile : listFiles){
                deleteFile(listFile);
            }
        }
        log.info("----------delete file:{}", file.getAbsolutePath());
        file.delete();
    }

    /**
     * 递归压缩方法
     * @param sourceFile 源文件
     * @param zos        zip输出流
     * @param name       压缩后的名称
     * @param keepDirStructure  是否保留原来的目录结构,true:保留目录结构;
     *                          false:所有文件跑到压缩包根目录下(注意：不保留目录结构可能会出现同名文件,会压缩失败)
     * @throws Exception
     */
    private static void compress(File sourceFile, ZipOutputStream zos, String name,
                                 boolean keepDirStructure) throws Exception{
        byte[] buf = new byte[BUFFER_SIZE];
        if(sourceFile.isFile()){
            // 向zip输出流中添加一个zip实体，构造器中name为zip实体的文件的名字
            zos.putNextEntry(new ZipEntry(name));
            // copy文件到zip输出流中
            int len;
            FileInputStream in = new FileInputStream(sourceFile);
            while ((len = in.read(buf)) != -1){
                zos.write(buf, 0, len);
            }
            // Complete the entry
            zos.closeEntry();
            in.close();
        } else {
            //是文件夹
            File[] listFiles = sourceFile.listFiles();
            if(listFiles == null || listFiles.length == 0){
                // 需要保留原来的文件结构时,需要对空文件夹进行处理
                if(keepDirStructure){
                    // 空文件夹的处理
                    zos.putNextEntry(new ZipEntry(name + "/"));
                    // 没有文件，不需要文件的copy
                    zos.closeEntry();
                }
            }else {
                for (File file : listFiles) {
                    // 判断是否需要保留原来的文件结构
                    if (keepDirStructure) {
                        // 注意：file.getName()前面需要带上父文件夹的名字加一斜杠,
                        // 不然最后压缩包中就不能保留原来的文件结构,即：所有文件都跑到压缩包根目录下了
                        compress(file, zos, name + "/" + file.getName(),keepDirStructure);
                    } else {
                        compress(file, zos, file.getName(),keepDirStructure);
                    }
                }
            }
        }
    }



    public static ArrayList unZipImagesFiles(String zipPath, String descDir) {
        File zipFile = new File(zipPath);
        ZipFile zip = null;
        ArrayList<String> activityFile = new ArrayList<>();
        try {
            File pathFile = new File(descDir);
            if(!pathFile.exists())
            {
                pathFile.mkdirs();
            }
            //解决zip文件中有中文目录或者中文文件
            zip = new ZipFile(zipFile, Charset.forName("UTF-8"));
            for(Enumeration entries = zip.entries(); entries.hasMoreElements();)
            {
                InputStream in = null;
                OutputStream out = null;
                try {
                    ZipEntry entry = (ZipEntry)entries.nextElement();
                    String zipEntryName = entry.getName();
//                    if( zipEntryName.endsWith(".txt") && zipEntryName.indexOf("/") == zipEntryName.lastIndexOf("/")){
                    activityFile.add( zipEntryName);
//                    }
                    in = zip.getInputStream(entry);
                    String outPath = (descDir+zipEntryName).replaceAll("\\*", "/");;
                    //判断路径是否存在,不存在则创建文件路径
                    File file = new File(outPath.substring(0, outPath.lastIndexOf('/')));
                    if(!file.exists())
                    {
                        file.mkdirs();
                    }
                    //判断文件全路径是否为文件夹,如果是上面已经上传,不需要解压
                    if(new File(outPath).isDirectory())
                    {
                        continue;
                    }
                    //输出文件路径信息
                    out = new FileOutputStream(outPath);
                    byte[] buf1 = new byte[1024];
                    int len;
                    while((len=in.read(buf1))>0)
                    {
                        out.write(buf1,0,len);
                    }
                }finally {
                    try {
                        if( null != in ){
                            in.close();
                        }
                        if( null != out ){
                            out.close();
                        }
                    }catch (Exception e){
                        log.error("关闭zip解压后文件流{}失败", zipFile.getAbsolutePath());
                    }
                }
            }
        }catch (Exception e){
            throw new CommonException(FileErrorEnum.ERROR_IMPORT_FILE);
        }finally {
            try {
                if( null != zip ){
                    zip.close();
                }
            }catch (Exception e){
                log.error("关闭zip文件流{}失败", descDir);
            }
        }
        return activityFile;
    }





    /**
     * 解压到指定目录
     * @return
     */
    public static List<String> unZipFiles(String zipPath, String descDir)
    {
        return unZipFiles(new File(zipPath), descDir);
    }

    /**
     * 解压到指定目录
     * @return
     */
    public static List<String> unZipFiles(File zipFile, String descDir)
    {
        ZipFile zip = null;
        List<String> activityFileList = new ArrayList<>();
        try {
            File pathFile = new File(descDir);
            if(!pathFile.exists())
            {
                pathFile.mkdirs();
            }
            //解决zip文件中有中文目录或者中文文件
            zip = new ZipFile(zipFile, Charset.forName("UTF-8"));
            for(Enumeration entries = zip.entries(); entries.hasMoreElements();) {
                InputStream in = null;
                OutputStream out = null;
                try {
                    ZipEntry entry = (ZipEntry)entries.nextElement();
                    String zipEntryName = entry.getName();
//                    if( zipEntryName.endsWith(".txt") && zipEntryName.indexOf("/") == zipEntryName.lastIndexOf("/")){
                    activityFileList.add(zipEntryName);
//                    }
                    in = zip.getInputStream(entry);
                    String outPath = (descDir+zipEntryName).replaceAll("\\*", "/");;
                    //判断路径是否存在,不存在则创建文件路径
                    File file = new File(outPath.substring(0, outPath.lastIndexOf('/')));
                    if(!file.exists())
                    {
                        file.mkdirs();
                    }
                    //判断文件全路径是否为文件夹,如果是上面已经上传,不需要解压
                    if(new File(outPath).isDirectory())
                    {
                        continue;
                    }
                    //输出文件路径信息
                    out = new FileOutputStream(outPath);
                    byte[] buf1 = new byte[1024];
                    int len;
                    while((len=in.read(buf1))>0)
                    {
                        out.write(buf1,0,len);
                    }
                }finally {
                    try {
                        if( null != in ){
                            in.close();
                        }
                        if( null != out ){
                            out.close();
                        }
                    }catch (Exception e){
                        log.error("关闭zip解压后文件流{}失败", zipFile.getAbsolutePath());
                    }
                }
            }
        }catch (Exception e){
            throw new CommonException(FileErrorEnum.ERROR_IMPORT_FILE);
        }finally {
            try {
                if( null != zip ){
                    zip.close();
                }
            }catch (Exception e){
                log.error("关闭zip文件流{}失败", descDir);
            }
        }
        return activityFileList;
    }

}
