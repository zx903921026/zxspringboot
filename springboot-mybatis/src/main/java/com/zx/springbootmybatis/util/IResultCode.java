package com.zx.springbootmybatis.util;

import java.io.Serializable;

/**
 * @author johnny.liu
 * @version 1.0
 * @date 2020/5/6 12:15
 */
public interface IResultCode extends Serializable {

    /**
     * 消息
     *
     * @return String
     */
    String getErrorMessage();

    /**
     * 状态码
     *
     * @return int
     */
    String getErrorCode();

}
