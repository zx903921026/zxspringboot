package com.zx.springbootmybatis.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
@Component
public class RedisUtil {
    @Autowired
    private  RedisTemplate stringRedisTemplate;



    public  boolean getAndTryLock(String key) {
        Boolean lock = stringRedisTemplate.opsForValue().setIfAbsent(key, "true",5, TimeUnit.SECONDS);
        if (lock) {
            return true;
        }else {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return getAndTryLock(key);
        }
    }
    public  void releaseLock(String messageId) {
        stringRedisTemplate.delete(messageId);
    }
}
