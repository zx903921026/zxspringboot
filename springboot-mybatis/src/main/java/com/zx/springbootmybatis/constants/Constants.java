package com.zx.springbootmybatis.constants;


/**
 * Created by kalend.zhang on 2020/2/27.
 *
 * @author kalend.zhang
 * @date 2020/2/27 下午2:41
 */
public interface Constants {


    interface Base {
        /**
         * The development environment
         */
        String DEV_CODE = "dev";
        /**
         * The com.zx.springbootmybatis.test environment
         */
        String TEST_CODE = "test";
        /**
         * The production environment
         */
        String PROD_CODE = "prod";
        /**
         * The qa environment
         */
        String QA_CODE = "qa";
    }

    interface Price {
        /**
         * Time object Time zone
         */
        String LOCAL_DATE_TIME_OFFSET = "+8";

        /**
         * Default price expiration time
         */
        String PRICE_DEFAULT_END_TIME = "2999-12-31 23:59:59";

        /**
         * Default sales Unit: 件
         */
        String DEFAULT_UNIT_PIECE = "件";
    }

    interface Channel {
        /**
         * type :on line
         */
        String ONLINE = "ONLINE";
        /**
         * type:off line
         */
        String OFFLINE = "OFFLINE";
        /**
         * The operation to add or update the database succeeds
         */
        Integer OPERATE_DATASOURCE_SUCCESS = 1;
        /**
         * approvalStatus：approved
         */
        String APPROVAL_STATUS = "APPROVED";
        String APPROVED = "APPROVED";
        /**
         * approvalStatus：unapproved
         */
        String APPROVAL_STATUS_NOT = "UNAPPROVED";
        /**
         * default channel
         */
        String DEFAULT = "DEFAULT" ;
    }

    interface Tag {
        String DISCOUNT = "DISCOUNT";
        String VIRTUAL = "VIRTUAL";
        String EFFECTIVE_DATE = "2000-01-01 00:00:01";
        String EXPIRE_DATE = "2099-12-31 23:59:59";
        /**
         * White list
         */
        String WHITE_PRODUCT = "whiteProduct";
//        String SEVEN_DISCOUNT_BUY = "SEVEN_DISCOUNT_BUY";
        String LIMIT_PURCHASE = "LIMIT_PURCHASE";
        String QUOTA = "QUOTA";
    }

    interface Sku {
        String CONVERT_SKU = "convertSku";
        String CONVERT_SUB_SKU = "convertSubSku";
    }
    interface SkuCntDescription {
        String COMBINED_SKU = "套组合任意选";
        String SKU = "种规格可选";
    }

    interface Product {
        String APPROVED = "APPROVED";
        String UNAPPROVED = "UNAPPROVED";
        /**
         * ACCL MicroPurchase 3E
         */
        String ACCL = "accl";

        /**
         * ACCL MicroPurchase 3E
         */
        String MICRO_BUY_3E_AC_CL = "5";

        /**
         * ACEC产品 海外购非微购产品
         */
        String OUT_AC_EC = "6";

        /**
         * ACEC 海外购微购
         */
        String MICRO_BUY_OUT_AC_EC = "7";

        /**
         * ACEC 微购产品
         */
        String MICRO_BUY_AC_EC = "8";


    }
    interface SceneType {
        String DEFAULT = "";
        String HP = "hp";
        String PDP = "pdp";
        String CART_PAGE = "cartpage";
        String ADDON_PAGE = "addonpage";
        String A_CATE_LOG_PAGE = "Acatelogpage";

        String ADDON_PAGE_PV ="addonpageforPV";

       /**
         * Scenario of Super App
         */
        String SALE_INDEX = "saleindex";
        String TRANS_IT_FREE = "transitfree";
        String TRANS_IT_FREE_OVERSEA = "transitfreeoversea";
        String REPUTATION = "reputation";
        String SURGE = "surge";
        String HOTTEST = "hottest";
        String ORDER_COMPLETION = "ordercompletion";
        String PDP_APP = "pdpapp";
        String REG_PURCHASE_LIST = "regpurchaselist";
        String CATE_LOG_APP = "catelogapp";
        String CATE_PAGE_APP = "cartpageapp";
        String HP_CONFIG = "hp_config";
        String LOTTERY_PAGE ="lotterypage";
        Integer DATA_GRAND_SIZE = 64;
        Integer ITEM_ID_SIZE = 10;


    }

    interface RecommendConstant{
        /**
         * default cateId of data grand, format as 1_2000, 2000 is category,rely on material upload format
         *
         */
        String DEFAULT_CATE_ID="1";

        String A_MALL="Amall";
        String SUPER_APP = "superapp";
        String RELATE= "relate";
        String PERSONAL = "personal";
        String HOT = "hot";
    }


    int TRUE = 1;

    int FALSE = 0;

    /**
     * success
     */
    String SUCCESS = "success";


    /**
     * http return code: success
     */
    String HTTP_RESPONSE_CODE_SUCCESS = "0";

    /**
     * delete or not: no
     */
    Integer IS_DELETED_FALSE = 0;

    /**
     * delete or not: yes
     */
    Integer IS_DELETED_TRUE = 1;

    /**
     * Operation success flag
     */
    Integer OPERATE_SUCCESS = 1;

    /**
     * Operation failed flag
     */
    Integer OPERATE_FAILURE = 0;

    /**
     * active
     */
    String ACTIVE = "ACTIVE";

    /**
     * inactive
     */
    String INACTIVE = "INACTIVE";

    /**
     * regular expressions of code
     */
    String CODE_REGEX = "^[\\x01-\\x7f]*$";

    /**
     * parameter exception information: code
     */
    String PARAM_VALIDATE_CODE = "code";

    /**
     * parameter exception information-正则表达式
     */
    String PARAM_VALIDATE_MESSAGE = "正则表达式";

    /**
     * parameter exception information-code不支持中文字符
     */
    String PARAM_VALIDATE_MESSAGE_CODE = "code不支持中文字符";

    /**
     * The maximum number of skuCode list
     */
    Integer MAX_SKU_CODE_LIST_SIZE = 20;

    /**
     * SKU CODE maximum length
     */
    Integer MAX_SKU_CODE_LENGTH = 50;

    Integer MATH_ZERO = 0;

    Integer MATH_ONE = 1;

    Integer MATH_TWO = 2;

    String STRING_ONE = "1";

    /**
     * main picture suffix(主图 plp 使用)
     */
    String MAIN_PICTURE_SUFFIX = "-1000Wx1000H-ATL-420Wx420H";

    /**
     * When querying the collection list：picture suffix（查询收藏列表时：图片后缀）
     */
    String COLLECTION_PICTURE_SUFFIX = "-1000Wx1000H-ATL-78Wx78H";

    /**
     * When query compose：picture suffix（查询搭配查询时：图片后缀）
     */
    String COMPOSE_PICTURE_SUFFIX = "-1000Wx1000H-ATL-280Wx280H";

    /**
     * share picture：picture suffix（分类页分享图：图片后缀）
     */
    String SHARE_PICTURE_SUFFIX = "-1000Wx1000H-ATL-280Wx280H";


    String CLASS_TYE = "class";

    String TAG_TYPE = "tag";

    String CATEGORY_TYPE = "category";

    String CATEGORY_PATH_LIST_CACHE = "category.path.mem.cache";

    interface Response {
        interface Message {
            String PREFIX = "error.commodity.message.";
        }
    }

    interface PageParam {
        String DESC = "desc";
    }

    /**
     * Sales attribute related constants
     */
    interface SaleAttribute {
        /**
         * common properties
         */
        String ATTRIBUTE_NAME = "attribute_name";
        /**
         * variant attribute name
         */
        String VARIANT_ATTRIBUTE_NAME = "variant_attribute_name";
        /**
         * multi dimension
         */
        String VARIANT_ATTRIBUTE_MULTI_NAME = "multi_dimension";
        /**
         * variant attribute logo
         */
        String VARIANT_ATTRIBUTE_LOGO = "variant_attribute_logo";
        /**
         * gift point
         */
        String GIFT_POINT = "gift_point";
        /**
         * mix logistics type name
         */
        String MIXOLOGISTICS_TYPE_NAME = "mixologistics_type_name";
        /**
         *  mix logistics product code
         */
        String MIXOLOGISTICS_AMPLIFIER_PRODUCT = "mixologistics_amplifier_product";
        /**
         * mix logistics product code
         */
        String MIXOLOGISTICS_AMPLIFIER_PRODUCT_TWO = "mixologistics_amplifier_product2";

        String SPECIFICATION = "规格";
        String TYPE = "类型";
        String PRODUCT = "产品";
        String GIFT_POINT_TYPE = "悦享分类型";
        String GIFT_POINT_TYPE_INTEGRAL = "悦享分：";
        String GIFT_POINT_TYPE_INTEGRAL_AND_CASH = "悦享分 + 现金：";
    }

    interface Currency {
        String RMB = "RMB";
        String USD = "USD";
    }

    /**
     * constants of Mixologistic
     */
    interface Mixologistic {
        /**
         * 由于雅姿的特殊性，5种只包含一瓶精华液的sku即可判断所有sku是否缺货
         * 避免库存调用此商品时查询25种sku
         */
        String[] SKUS_FOR_STOCK = new String[]{"9801", "9802", "9803", "9804", "9805",
                "9806", "9807", "9808", "9809", "9810",
                "9811", "9812", "9813", "9814", "9815",
                "9816", "9817", "9818", "9819", "9820",
                "9821", "9822", "9823", "9824", "9825"};
        /**
         * 雅姿特殊商品ES id
         */
        String MIXOLOGISTIC_CODE_PREFIX = "MIXOLOGISTIC_PREFIX_";
        /**
         * 雅姿组合的商品编码
         */
        String MIX_SPECIAL_PRODUCT = "mix_40461";
        /**
         * 雅姿组合的商品业务编码
         */
        String MIX_SPECIAL_PRODUCT_ORIGIN = "40461";

        /**
         * 雅姿默认sku
         */
        String MIX_SPECIAL_PRODUCT_DEFAULT_SKU = "9801";
    }

    String SC0A = "SC0A";

    /**
     * Search for constants such as field names or index aliases used internally
     */
    interface EsSearch {
        interface SortProperty {
            String DEFAULT = "DEFAULT";
            String NAME = "NAME";
            String PRICE = "PRICE";
            String GIFT_POINT = "GIFT_POINT";
            String PRODUCT_CODE = "PRODUCT_CODE";
            String SALE_COUNT = "SALE_COUNT";
            String SALE_TIME = "SALE_TIME";
            String HOT_SALE_SORT = "HOTSALE_SORT";
            String NEW_PRODUCT_SORT = "NEW_PRODUCT_SORT";
        }

        /**
         * price section type
         */
        interface PriceSectionType {
            String MONEY = "MONEY";
            String GIFT_POINT = "GIFT_POINT";
        }

        interface PriceType {
            String MONEY = "MONEY";
            String GIFT_POINT = "GIFT_POINT";
            String MONEY_AND_GIFT_POINT = "MONEY_AND_GIFT_POINT";
        }

        interface CategoryAggregateType {
            /**
             * return all
             */
            String ALL = "ALL";
            /**
             * return only the root directory
             */
            String ROOT = "ROOT";
            /**
             * returns the subclass of the current class
             */
            String CHILDREN = "CHILDREN";
            /**
             * returns the sibling node of the current class
             */
            String BROTHER = "BROTHER";
        }

        /**
         * ES automatically prompts you to name the key you want.
         * It has no practical significance. It is only used to obtain the suggest object.
         *
         * The ES supports multiple suggest requests at the same time.
         * Therefore, you need to distinguish them by name.
         */
        String SUGGEST_NAME = "productNameCompletion";

        /**
         * The name of the key required for ES aggregation is meaningless.
         * It is used only to obtain the aggregate object.
         *
         * The ES supports multiple aggregate requests at the same time.
         * Therefore, you need to distinguish them by name.
         */
        String PRICE_AGGREGATE = "pricesAggregate";

        String NESTED_PRICE_AGGREGATE = "nestedPricesAggregate";

        String CATEGORY_AGGREGATE = "categoryAggregate";

        // product function
        String TAG_AGGREGATE = "tagAggregate";

        String NESTED_TAG_AGGREGATE = "nestedTagAggregate";

        String NESTED_CATEGORY_AGGREGATE = "nestedcategoryAggregate";

        String OTHER_AGGREGATE = "otherAggregate";

        String NESTED_OTHER_AGGREGATE = "nestedOtherAggregate";

        /**
         * The name of the field of type Completion in ES
         * A separate index has been used and the field name is keyword
         */
        String SUGGEST_PROPERTY_NAME = "keyword";

        String DOT = ".";

        /**
         * This object contains pricing information about channels and roles.
         */
        String PRICE_LIST = "priceList";

        String DEFAULT_PRICE = "defaultPrice";

        String DEFAULT_PRICE_LIST = "defaultPriceList";

        /**
         * This object is a list of category paths
         * format as: [[A,B,C],[A,D,E]]
         */
        String CATEGORY_PATH_LIST = "categoryPathList";

        String TAG_LIST = "tagList";
        String LABELS = "labels";

        String CHANNEL_LIST = "channelList";
        String ORDER_ABLE = "orderable";

        /**
         * The following are the field names required for the search
         */
        String ITEM_CODE = "itemCode";
        String ID = "id";

//        String SKU_CODE_LIST = "skuCodeList";

        String ITEM_CODE_LIST = "itemCodeList";

        String PRODUCT_CODE = "productCode";

        String SALE_COUNT = "saleCnt";

        String SKU_CODE = "skuCode";

        String PRODUCT_NAME = "productName";

        String CHANNEL_CODE = "channelCode";

        String ROLE = "role";

        String INACTIVE = "inactive";

        String COMBINED_TYPE = "combinedType";

        String SEARCHABLE = "searchable";
        String SEARCHABLE_CHANNEL_CODES = "searchableChannelCodes";
        String NONE_SEARCHABLE_CHANNEL_CODES = "noneSearchableChannelCodes";
        String IS_MICRO_BUY = "isMicroBuy";
        String DEFAULT_SKU = "defaultSku";
        String PLP_SEARCHABLE = "plpSearchable";

        String PRICE = "price";
        String GIFT_POINT = "giftPoint";
        String PRICE_START_TIME = "startTime";
        String PRICE_TYPE = "priceType";
        String PRICE_SECTION = "priceSection";
        String OTHER_SECTION = "otherSection";
        String GIFT_POINT_SECTION = "giftPointSection";
        String CURRENCY = "currency";

        String BASE_PRICE = "basePrice";

        String NAME = "name";
        String NAME_FOR_ORDER = "nameForOrder";
        String MICRO_BUY_SORT = "microBuySort";
        String NAME_FOR_SORT = "nameForSort";
        String PLP_SORT = "plpSort";

        String SUMMARY = "summary";
        String KEYWORD_LIST = "keywordList";
        String SKU_NAME_LIST = "skuNameList";
        String MULTI_LANGUAGE_SKU_NAME = "multiLanguageSkuNameList";
        String MULTI_LANGUAGE_PRODUCT_NAME = "multiLanguageProductNameList";
        String MULTI_LANGUAGE_CONTENT = "multiLanguageContentList";
        String LANGUAGE = "language";

        String CATEGORY = "categoryCode";
        String CLASS_CODE = "classCode";
        String TAG_CODE = "code";
        String GROUP_BUY_CODE = "groupBuyCode";
        String VIRTUAL_PRODUCT_TYPE="virtualProductType";
        String VISIBLE_BEFORE_LOGIN = "visibleBeforeLogin";

        String KEY = "key";
        String VALUE = "value";
        String LABEL_WHITE = "labels";
        String CATEGORYCODE_LIST = "categoryCodeList";
        String CODE = "code";
        String HOT_SALE_SORT_VALUE = "hotSaleSortValue";
        String NEW_PRODUCT_SORT_VALUE = "newProductSort";

        interface Channel {
            String CHANNELCODE = "channelCode";
            String SHOPCODE = "shopCode";
            String ORDER_TYPE = "orderType";
            String APPROVAL_STATUS = "approvalStatus";
        }

        interface CategorySearch {

            String QUERY_ROOT_CODE = "brand";
            String ROOT_CODE = "nav";
            Long ROOT_ID = -1L;

            String CODE = "code";

            String PARENT_CODE = "parentCode";

            String PARENT_ID = "parentId";

            String LAYER = "layer";

            String SORT = "sort";

            String STATUS = "status";

            String CHANNEL_CODE = "channelCode";
        }

        interface Company{
            /**
             * default AC
             */
            String COMPANY_AC = "AC";

            /**
             * AM
             */
            String COMPANY_AM = "AM";
        }

    }

    interface ExtApi {

        String EXTAPIHEAD = "head";

        String EXTAPIFROM = "from";

    }

    interface Sync {

        Integer FULL_DATA = 1;

        Integer INCREMENT_DATA = 0;

    }
    interface ErrorLog {
        String SKU_CHANGE_ERROR = "sku_change";
        String TAG_SYNC_ERROR = "tag_sync";
        String ERROR_PRODUCT = "PRODUCT_ES_SYNC";
        String ERROR_CATEGORY = "CATEGORY_ES_SYNC";
    }
    interface CatchException {
        /**
         * The start identifier of database exception text
         */
        String DATA_ACCESS_EXCEPTION_TEXT_START = "truncation";
        /**
         * The end identifier of database exception text
         */
        String DATA_ACCESS_EXCEPTION_TEXT_END = "at row";
        /**
         * The length of database exception text
         */
        Integer DATA_ACCESS_EXCEPTION_TEXT_START_EXTEND = 12;

    }
    interface SyncType {
        String ATL_SYNC_TYPE="1";
    }


    interface NeedSyncChannelCode {
        String CHANNEL_CODE_INT = "INT";
        String CHANNEL_CODE_HUB = "HUB";
        String CHANNEL_CODE_WEC = "WEC";
        String CHANNEL_CODE_WEC_MP = "WEC_MP";
        String CHANNEL_CODE_AAS = "AAS";
        String CHANNEL_CODE_AEC = "AEC";
        String CHANNEL_CODE_DEFAULT = "DEFAULT";
        String CHANNEL_CODE_INTERNAL = "INTERNAL";
    }

    interface VirtualProductType {
        String WARRANTY = "WARRANTY";
    }

    interface CategoryProductRel {
        /**
         * add/update database operation success
         */
        Integer OPERATE_DATASOURCE_SUCCESS = 1;
    }

    interface Category {
        String BRAND = "brand";
    }

    interface SearchCenter{
        String KEYWORD = "keyword";
        String PAGE_NO = "pageNo";
        String PAGE_SIZE = "pageSize";
        String START_INDEX = "startIndex";
        String SORT_PROPERTY = "sortProperty";
        String SORT_TYPE = "sortType";
        String SUMMARY = "summary";
        String PRODUCT_CHANNEL = "productChannel";
        String FROM_REQUEST_ID = "fromRequestId";
        String FLOW_DIVIDER = "flowDivider";
        String SUMMARY_TAG = "summaryTag";
    }

    interface TagCode {
        String RENEW = "RENEW";
        String VARINT = "VARINT";
    }

    interface AttributeCode {
        String VISIBLE_BEFORE_LOGIN = "visible_before_login";
        String HOT_SALE_SORT = "hotsale_sort";
        String NEW_PRODUCT_SORT = "new_product_sort";
    }

    interface MQRetryScene {
        String CATEGORY_SYNC_FAIL = "CATEGORY_SYNC_FAIL";
        String PRODUCT_REFRESH_ES_FAIL = "PRODUCT_REFRESH_ES_FAIL";
        String COMBINED_SKU_SYNC_FAIL = "COMBINED_SKU_SYNC_FAIL";
        String PRODUCT_MASTER_DATA_SYNC_FAIL = "PRODUCT_MASTER_DATA_SYNC_FAIL";
        String PRODUCT_PRICE_SYNC_FAIL = "PRODUCT_PRICE_SYNC_FAIL";
        String TAG_SYNC_FAIL = "TAG_SYNC_FAIL";
    }

    interface MQLogScene {
        String PRODUCT_MASTER_DATA_SYNC = "PRODUCT_MASTER_DATA_SYNC";
        String PRODUCT_PRICE_SYNC = "PRODUCT_PRICE_SYNC";
    }
}
