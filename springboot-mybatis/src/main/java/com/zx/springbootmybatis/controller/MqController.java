package com.zx.springbootmybatis.controller;

import com.zx.springbootmybatis.mq.defaultsend.provider.DefaultProvider;
import com.zx.springbootmybatis.mq.delayMq.DelayProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MqController {

    @Autowired
    DefaultProvider provider;
    @Autowired
    DelayProducer delayProducer;

    @PostMapping("/sendmsg")
    public void sendmsg(@RequestParam String msg){
//        provider.send(msg);

        delayProducer.sendEmail();

    }
}
