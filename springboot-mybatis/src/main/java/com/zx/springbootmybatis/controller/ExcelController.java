package com.zx.springbootmybatis.controller;

import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import cn.afterturn.easypoi.excel.entity.enmus.ExcelType;
import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelReader;
import com.alibaba.excel.read.builder.ExcelReaderBuilder;
import com.alibaba.excel.read.listener.ReadListener;
import com.google.common.base.Throwables;
import com.zx.springbootmybatis.dto.ExcelDTO;
import com.zx.springbootmybatis.dto.request.ImportRequestDTO;
import com.zx.springbootmybatis.dto.response.ImportReturnDTO;
import com.zx.springbootmybatis.dto.response.StudentDTO;
import com.zx.springbootmybatis.error.CommonErrorEnum;
import com.zx.springbootmybatis.exception.CommonException;
import com.zx.springbootmybatis.model.ResultResponse;
import com.zx.springbootmybatis.model.User;
import com.zx.springbootmybatis.util.ExcelDTOListener;
import com.zx.springbootmybatis.util.ExcelUtil;
import lombok.extern.log4j.Log4j;
import lombok.extern.log4j.Log4j2;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLEncoder;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Log4j2
@RestController
public class ExcelController {
    @Resource
    ExcelUtil excelUtil;
    @PostMapping("/importExcel")
    public ResultResponse importExcel(ImportRequestDTO importLandingRequest) throws IOException {
        List<ExcelDTO> excelData=new ArrayList<>();
        try {
            excelData = excelUtil.getExcelData(importLandingRequest.getFile().getInputStream(), importLandingRequest.getFile().getOriginalFilename());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResultResponse.succResult(excelData);
    }


    @PostMapping("/importExcel2")
    public ResultResponse importExcel2(ImportRequestDTO importLandingRequest) throws IOException {
        List<ExcelDTO> excelData=new ArrayList<>();
        try {
            ExcelReaderBuilder read = EasyExcel.read(importLandingRequest.getFile().getInputStream(), ExcelDTO.class, new ExcelDTOListener());
            read.sheet().doRead();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResultResponse.succResult(excelData);
    }

//    @GetMapping ("/poiExportExcel")
//    public ResultResponse poiExportExcel(HttpServletResponse response) throws IOException {
//        List<CouponSendDO> categoryDOList = couponSendDao.selectByExample(requestDTO);
//        String[] titles={"安利卡号","手机号码","人员身份","单张金额","发放张数","生效日期","截止日期","生成电子券原始编码","发放人中文名","发入人CN账号"};
//        exportExcel(categoryDOList, titles,response);
//    }
//    public void exportExcel(List<CouponSendDO> list, String[] titles,HttpServletResponse response) {
//        ServletOutputStream os=null;
//        SXSSFWorkbook wb = new SXSSFWorkbook(100);
//        Sheet sheet = wb.createSheet();
//        Row row = sheet.createRow(0);
//        //给单元格设置样式
//        CellStyle cellStyle = wb.createCellStyle();
//        Font font = wb.createFont();
//        //设置字体大小
//        font.setFontHeightInPoints((short) 12);
//        //设置字体加粗
//        font.setBold(true);
//        //给字体设置样式
//        cellStyle.setFont(font);
//        //设置单元格背景颜色
//        cellStyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
//        //设置单元格填充样式(使用纯色背景颜色填充)
//        cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
//        for (int i = 0; i < titles.length; i++) {
//            Cell cell = row.createCell(i);
//            cell.setCellValue(titles[i]);
//            cell.setCellStyle(cellStyle);
//            //设置列的宽度
//            sheet.setColumnWidth(i, 200*50);
//        }
//        DateTimeFormatter sdf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
//        for (int j = 0; j < list.size(); j++) {
//            Row rowData = sheet.createRow(j + 1);
//            CouponSendDO couponSendDO = list.get(j);
//            Cell cell = rowData.createCell(0);
//            cell.setCellValue(couponSendDO.getAda());
//            Cell cell2 = rowData.createCell(1);
//            cell2.setCellValue(couponSendDO.getMobile());
//            Cell cell3 = rowData.createCell(2);
//            cell3.setCellValue(couponSendDO.getIdentityType());
//            Cell cell4 = rowData.createCell(3);
//            cell4.setCellValue(couponSendDO.getDp());
//            Cell cell5 = rowData.createCell(4);
//            cell5.setCellValue(couponSendDO.getCouponNumber());
//            Cell cell6 = rowData.createCell(5);
//
//            cell6.setCellValue(sdf.format(couponSendDO.getEffectiveDateStart()));
//            Cell cell7 = rowData.createCell(6);
//            cell7.setCellValue(sdf.format(couponSendDO.getEffectiveDateEnd()));
//            Cell cell8 = rowData.createCell(7);
//            cell8.setCellValue(couponSendDO.getCouponType());
//            Cell cell9 = rowData.createCell(8);
//            cell9.setCellValue(couponSendDO.getAuditUser());
//            Cell cell10 = rowData.createCell(9);
//            cell10.setCellValue(couponSendDO.getAuditId());
//
//        }
//
//        try {
//            // 附件名称和格式
//            response.addHeader("Content-Disposition","attachment;filename="+ URLEncoder.encode("电子券派发明细表.xlsx","UTF-8"));
//            response.setContentType("application/vnd.ms-excel");
//            // 转为二进制流进行吐出
//            os = response.getOutputStream();
//            wb.write(os);
//        } catch (Exception e) {
//            log.error("Excel error", e);
//        }finally {
//            try {
//                os.flush();
//                os.close();
//            } catch (IOException e) {
//                e.printStackTrace();
//                //return new ResultMoudel("error", "IO流关闭异常");
//            }
//        }
//    }

    public SXSSFWorkbook exportExcel(List<StudentDTO> list, String[] titles, HttpServletResponse response) {
        SXSSFWorkbook wb = new SXSSFWorkbook(100);
        Sheet sheet = wb.createSheet();
        Row row = sheet.createRow(0);
        //给单元格设置样式
        CellStyle cellStyle = wb.createCellStyle();
        Font font = wb.createFont();
        //设置字体大小
        font.setFontHeightInPoints((short) 12);
        //设置字体加粗
        font.setBold(true);
        //给字体设置样式
        cellStyle.setFont(font);
        //设置单元格背景颜色
        cellStyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
        //设置单元格填充样式(使用纯色背景颜色填充)
        cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        for (int i = 0; i < titles.length; i++) {
            Cell cell = row.createCell(i);
            cell.setCellValue(titles[i]);
            cell.setCellStyle(cellStyle);
            //设置列的宽度
            sheet.setColumnWidth(i, 200*50);
        }
//        return wb;
        for (int j = 0; j < list.size(); j++) {
            Row rowData = sheet.createRow(j + 1);
            StudentDTO couponSendDO = list.get(j);
            Cell cell = rowData.createCell(0);
            cell.setCellValue(couponSendDO.getId());
            Cell cell2 = rowData.createCell(1);
            cell2.setCellValue(couponSendDO.getAge());
            Cell cell3 = rowData.createCell(2);
            cell3.setCellValue(couponSendDO.getName());
        }
        // 每次创建一行一列都需要调用方法可自己加工成工具类使用时只需要赋值即可
        try {
            // 附件名称和格式
            response.addHeader("Content-Disposition","attachment;filename="+ URLEncoder.encode("学生信息.xlsx","UTF-8"));
            response.setContentType("application/vnd.ms-excel");
            // 转为二进制流进行吐出
            ServletOutputStream os = response.getOutputStream();
            wb.write(os);
            os.flush();
            os.close();
        } catch (Exception e) {
//            log.("Excel error", e);
        }
        return wb;
    }




    public void exportExcel2(List<StudentDTO> list, String[] titles, HttpServletResponse response) {
        try {

            List<StudentDTO>studentDTOList=new ArrayList<>();

            String fileName = "电子券派发明细表";
            ExportParams exportParams = new ExportParams();
            exportParams.setSheetName(fileName);
            exportParams.setType(ExcelType.XSSF);
            Workbook workbook = ExcelExportUtil.exportExcel(exportParams, StudentDTO.class, studentDTOList);
            response.setCharacterEncoding("UTF-8");
            response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(fileName + ".xlsx", "UTF-8"));
            response.setHeader("content-Type", "application/vnd.ms-excel");
            workbook.write(response.getOutputStream());
        } catch (Exception e) {
            log.error("运营-导出电子券派发明细失败：{}", Throwables.getStackTraceAsString(e));
//            throw new CommonException();
        }

    }










    /**
     excel文件的下载
     */
    @GetMapping("download")
    public void download(HttpServletResponse response) throws IOException {
//        response.setContentType("application/vnd.ms-excel");
//        response.setCharacterEncoding("utf-8");
//        response.setHeader("Content-disposition", "attachment;filename=demo.xlsx");
////        EasyExcel.write(response.getOutputStream(), ExcelDTO.class).sheet("模板").doWrite(data());
        List list=new ArrayList();
        ExcelDTO dto=new ExcelDTO();
        dto.setColumn1("11");
        dto.setColumn2("22");
        dto.setColumn3("33");
        dto.setColumn4("44");
        list.add(dto);

        response.setContentType("application/vnd.ms-excel");
        response.setCharacterEncoding("utf-8");
        // 这里URLEncoder.encode可以防止中文乱码 当然和easyexcel没有关系
        String fileName = URLEncoder.encode("数据写出", "UTF-8");
        response.setHeader("Content-disposition", "attachment;filename=" + fileName + ".xlsx");
        EasyExcel.write(response.getOutputStream(), ExcelDTO.class).sheet("模板").doWrite(list);

    }


}
