package com.zx.springbootmybatis.controller;

import com.google.common.hash.BloomFilter;
import com.google.common.hash.Funnels;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.scripting.support.ResourceScriptSource;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

@RestController
public class RedisController {
    @Autowired
    private RedisTemplate redisTemplate;

    @RequestMapping("/getname")
    public String getname(@RequestParam String key,String value){
        redisTemplate.opsForValue().set(key,value);
        Object o = redisTemplate.opsForValue().get(key);
        return o.toString();
    }

    @PostMapping("/redisLua")
    public String redisLua(@RequestParam String key,String value) {
        DefaultRedisScript<Object> redisScript = new DefaultRedisScript<>();
        redisScript.setScriptSource(new ResourceScriptSource(new ClassPathResource("/lua/batchAdd.lua")));
//        redisScript.setResultType(returnType);
        List<String> keys=new ArrayList();
        List<String> values=new ArrayList();
        for (int i = 0; i < 10; i++) {
            keys.add(String.valueOf(i));
            values.add(String.valueOf(i));
        }

        redisTemplate.execute(redisScript, keys, values);
        return null;
    }


    @PostMapping("/redisLuadel")
    public String redisLuadel(@RequestParam String key,String value) {
        DefaultRedisScript<Object> redisScript = new DefaultRedisScript<>();
        redisScript.setScriptSource(new ResourceScriptSource(new ClassPathResource("/lua/batchDel.lua")));
//        redisScript.setResultType(returnType);
        List keys=new ArrayList();
        List values=new ArrayList();
        for (int i = 0; i < 10; i++) {
            keys.add(String.valueOf(i));
            values.add("zx".concat(String.valueOf(i)));
        }
        String a="2";
        redisTemplate.execute(redisScript, keys, null);
        return null;
    }
    @PostMapping("/bloomFilter")
    public String bloomFilter(@RequestParam String key ) {
        /**
         * 创建一个插入对象为一亿，误报率为0.01%的布隆过滤器
         * 不存在一定不存在
         * 存在不一定存在
         * ----------------
         *  Funnel 对象：预估的元素个数，误判率
         *  mightContain ：方法判断元素是否存在
         */

        BloomFilter<CharSequence> bloomFilter = BloomFilter.create(Funnels.stringFunnel(Charset.forName("utf-8")), 100000000, 0.0001);
        bloomFilter.put("死");
        bloomFilter.put("磕");
        bloomFilter.put("Redis");
        System.out.println(bloomFilter.mightContain("Redis"));
        System.out.println(bloomFilter.mightContain("Java"));

    //的原理是这样子的：将数据库中所有的查询条件，放入布隆过滤器中，当一个查询请求过来时，
    // 先经过布隆过滤器进行查，如果判断请求查询值存在，则继续查；如果判断请求查询不存在，直接丢弃。
        String value = (String) redisTemplate.opsForValue().get(key);
        if (value == null) {
            if (bloomFilter.mightContain(key)) {
                return null;
            } else {
//                value = db.get(key);
                redisTemplate.opsForValue().set(key, value);
            }
        }
        return value;
    }


}
