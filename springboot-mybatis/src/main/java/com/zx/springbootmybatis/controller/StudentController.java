package com.zx.springbootmybatis.controller;

import com.zx.springbootmybatis.dto.response.StudentDTO;
import com.zx.springbootmybatis.model.StudentDO;
import com.zx.springbootmybatis.service.StudentService;
import com.zx.springbootmybatis.util.ConvertorUtils;
import ma.glasnost.orika.MapperFacade;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class StudentController {
    @Autowired
    StudentService studentService;
    @Autowired
    private MapperFacade mapperFacade;

    @RequestMapping(value = "/student")
    public StudentDTO student(@RequestParam Integer id){
        StudentDO studentDO=studentService.getstudentById(id);
        StudentDTO studentDTO=new StudentDTO();
//        studentDTO= mapperFacade.map(studentDO,StudentDTO.class);
        StudentDTO studentDTO2 = ConvertorUtils.mapAsObj(studentDO, StudentDTO.class);
        return studentDTO;
    }

    @RequestMapping(value = "/updateStudent" ,method = RequestMethod.POST)
    public int updateStudent(@RequestParam Integer id,String name){
        StudentDO student=new StudentDO();
        student.setId(id);
        student.setName(name);
        int updateCount=studentService.updateStudent(student);
        return updateCount;
    }

    @RequestMapping(value = "/student/detail/{id}/{name}"  )
    public StudentDO student2(@PathVariable("id") Integer id,
                        @PathVariable("name") String name){
        StudentDO student=new StudentDO();
        student.setId(id);
        student.setName(name);
        return student;
    }

    @GetMapping(value = "/student/getstr")
    public String getstr( ){
        String getstr="skucode与文件夹名不符23124-1-1200Wx1200H.jpg<br/>skucode与文件夹名不符23124-2-1200Wx1200H.jpg</br>skucode与文件夹名不符23124-3-1200Wx1200H.jpg<br>skucode";
        return getstr;
    }


}
