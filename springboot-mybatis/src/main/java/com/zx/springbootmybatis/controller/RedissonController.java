//package com.zx.springbootmybatis.controller;
//
//import org.redisson.Redisson;
//import org.redisson.api.RLock;
//import org.redisson.api.RLongAdder;
//import org.redisson.api.RedissonClient;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RestController;
//
//import java.util.concurrent.TimeUnit;
//
//@RestController
//public class RedissonController {
//    @Autowired
//    private RedissonClient redisson;
//
//
//    @RequestMapping("/redissontest")
//    public void redissontest(@RequestParam String key, String value) throws InterruptedException {
////        默认，非公平锁
//        RLock anylock = redisson.getLock("anylock");
//        anylock.lock(10, TimeUnit.SECONDS);
////        第一个指定最长等待时间waitTime，第二个指定最长持有锁的时间 holdTime, 第三个是单位
//        boolean b = anylock.tryLock(100, 10, TimeUnit.SECONDS);
//        if(b){
//            try {
////                todo
//            }finally {
//                anylock.unlock();
//            }
//        }
//    }
//
//
//
//    @RequestMapping("/redissontest")
//    public void redissontest2(@RequestParam String key, String value) throws InterruptedException {
////        LongAdder
////        这个类的功能类似于AtomicLong，但是LongAdder的高并发时性能会好很多，非常适合高并发时的计数。（DoubleAdder类似）
//        RLongAdder longAdder = redisson.getLongAdder("myLongAdder");
//        longAdder.add(12);
//        longAdder.increment();
//        longAdder.decrement();
//        longAdder.sum();
//    }
//}
