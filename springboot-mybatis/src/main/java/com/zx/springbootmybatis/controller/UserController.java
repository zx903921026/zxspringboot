package com.zx.springbootmybatis.controller;

import com.zx.springbootmybatis.error.CommonErrorEnum;
import com.zx.springbootmybatis.exception.CommonException;
import com.zx.springbootmybatis.model.User;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
public class UserController {
    @PostMapping("user/login")
    public void login(@RequestParam String name , HttpServletRequest request){
        User user=new User();
        user.setName("zx");
        user.setPassword("123");
        request.getSession().setAttribute("user",user);
    }

    @RequestMapping("user/error")
    public String error(){
        throw new CommonException(CommonErrorEnum.UNKNOWN_EXCEPTION,"zxzxzx");

//        return "错误";
    }
    @RequestMapping("user/out")
    public String out(){
        return "out";
    }
    @RequestMapping("out")
    public String out2(){
        return "out";
    }
}
