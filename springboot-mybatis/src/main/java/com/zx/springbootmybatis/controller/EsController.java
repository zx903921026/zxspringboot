package com.zx.springbootmybatis.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.tools.json.JSONUtil;
import com.zx.springbootmybatis.es.dao.EmployeeDAO;
import com.zx.springbootmybatis.es.esModel.EmployeeBean;
import com.zx.springbootmybatis.es.esModel.MultiLanguageModel;
import com.zx.springbootmybatis.es.repository.EmployeeRepository;
import lombok.val;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.text.Text;
import org.elasticsearch.common.unit.Fuzziness;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.*;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.aggregations.AggregationBuilder;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightField;
import org.elasticsearch.search.sort.SortBuilder;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.data.elasticsearch.core.SearchResultMapper;
import org.springframework.data.elasticsearch.core.aggregation.AggregatedPage;
import org.springframework.data.elasticsearch.core.aggregation.impl.AggregatedPageImpl;
import org.springframework.data.elasticsearch.core.query.*;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

@RestController
public class EsController {
    @Autowired
    private ElasticsearchRestTemplate restTemplate;
    @Autowired
    RestHighLevelClient client;
    @Autowired
    EmployeeDAO employeeDAO ;
    @Autowired
    private EmployeeRepository repository;

    @PostMapping("/aggregation")
    public AggregatedPage<EmployeeBean> aggregation() throws IOException {
        TermQueryBuilder queryBuilder = QueryBuilders.termQuery("name", "zx");
        //分页
        Sort sort = Sort.by(Sort.Direction.DESC, "id");
        Pageable pageRequest = PageRequest.of(0, 2, sort);
        Iterable<EmployeeBean> search2 = repository.search(queryBuilder);

        SearchQuery  searchQuery=new NativeSearchQueryBuilder()
                .withQuery(QueryBuilders.matchAllQuery())
                .withHighlightFields(new HighlightBuilder.Field("name").preTags("<font color='red'>").postTags("</font>"))//设置高亮效果
                .withHighlightFields(new HighlightBuilder.Field("desc").preTags("<font color='red'>").postTags("</font>"))
                .build();
        Page<EmployeeBean> search = repository.search(searchQuery);
        return null;
    }


    @PostMapping("/aggregation2")
    public AggregatedPage<EmployeeBean> aggregation2() throws IOException {
        TermQueryBuilder queryBuilder = QueryBuilders.termQuery("name", "zx");
        //分页
        Sort sort = Sort.by(Sort.Direction.DESC, "id");
        Pageable pageRequest = PageRequest.of(0, 2, sort);
        Iterable<EmployeeBean> search2 = repository.search(queryBuilder);

        SearchQuery  searchQuery=new NativeSearchQueryBuilder()
                .withQuery(QueryBuilders.matchAllQuery())
                .withHighlightFields(new HighlightBuilder.Field("name").preTags("<font color='red'>").postTags("</font>"))//设置高亮效果
                .withHighlightFields(new HighlightBuilder.Field("desc").preTags("<font color='red'>").postTags("</font>"))
                .build();
        Page<EmployeeBean> search = repository.search(searchQuery);
        return null;
    }











    @PostMapping("/highlight")
    public AggregatedPage<EmployeeBean> highlight() throws IOException {
//        SearchQuery  searchQuery=new NativeSearchQueryBuilder()
//                .withQuery(QueryBuilders.matchAllQuery())
//                .withHighlightFields(new HighlightBuilder.Field("name").preTags("<font color='red'>").postTags("</font>"))//设置高亮效果
//                .withHighlightFields(new HighlightBuilder.Field("desc").preTags("<font color='red'>").postTags("</font>"))
//                .build();
//
//        AggregatedPage<EmployeeBean> list = restTemplate.queryForPage(searchQuery, EmployeeBean.class,new SearchResultMapper() {
//            @Override
//            public <T> AggregatedPage<T> mapResults(SearchResponse searchResponse, Class<T> aClass, Pageable pageable) {
//                List<EmployeeBean> list = new ArrayList<>();
//                for(SearchHit  hit:searchResponse.getHits()){//获取遍历查询结果
//                    if(searchResponse.getHits().getHits().length<=0)return null;
//                    EmployeeBean bean=new EmployeeBean();
//                    Map map=hit.getSourceAsMap();
//                    System.out.println(map);
//                    bean.setId((String) map.get("id"));
//                    bean.setName((String)map.get("name"));
//                    HighlightField name=hit.getHighlightFields().get("name");
//                    if(name!=null){
//                        bean.setName(name.fragments()[0].toString());   //得到高亮的结果
//                    }
//                    HighlightField desc=hit.getHighlightFields().get("desc");
//                    if(desc!=null){
//                        bean.setDesc(desc.fragments()[0].toString());   //得到高亮的结果
//                    }
//                    list.add(bean);
//                }
//                if(list.size()>0)return new AggregatedPageImpl<>((List<T>)list);
//
//                return null;
//            }
//
//            @Override
//            public <T> T mapSearchHit(SearchHit searchHit, Class<T> aClass) {
//                return null;
//            }
//        });
//
//        return list;
        return null;
    }
    private String highlightFieldToString(HighlightField field) {
        Text[] texts = field.getFragments();
        StringBuilder sb = new StringBuilder();
        for (Text text : texts) {
            sb.append(text.string());
        }
//        return subStr(sb.toString());
        return (sb.toString());

    }

    @PostMapping("/fuzyquery")
    public List<EmployeeBean> fuzyquery(){
        FuzzyQueryBuilder queryBuilder=QueryBuilders.fuzzyQuery("name","zx").fuzziness(Fuzziness.ONE);

        SearchQuery searchQuery=new NativeSearchQuery(queryBuilder);
        List<EmployeeBean> list = restTemplate.queryForList(searchQuery, EmployeeBean.class);
        return list;
    }
    @PostMapping("/save")
    public void save() {
//            保存数据
        EmployeeBean bean = new EmployeeBean();
        bean.setId("4");
        bean.setName("中国济南");
        bean.setMobile("1234");
        bean.setType(1);
        bean.setDesc("中国济南格式萨芬Abc");
        bean.setStudentCode("111222");
        List<MultiLanguageModel> list = new ArrayList<>();
        MultiLanguageModel model = new MultiLanguageModel();
        model.setName("中国济南");
        model.setLanguage("cn");
        model.setCode("11");
        list.add(model);
        bean.setMultiLanguageSkuNameList(list);
        repository.save(bean);
    }

    @PostMapping("/textquery")
    public List<EmployeeBean> textquery() {
//        NativeSearchQueryBuilder queryBuilder=new NativeSearchQueryBuilder();
//        queryBuilder.withQuery(new MatchPhraseQueryBuilder("desc", "bc"));
//        NativeSearchQuery query=queryBuilder.build();
        MatchQueryBuilder matchPhraseQueryBuilder = QueryBuilders.matchQuery("desc", "c");
        NativeSearchQuery query=new NativeSearchQuery(matchPhraseQueryBuilder);
        Page<EmployeeBean> search = repository.search(query);
        return search.getContent();
    }



    @PostMapping("/boolquery")
    public List<EmployeeBean> boolquery(){
//        组合查询

        //范围查询
        RangeQueryBuilder queryBuilder=new RangeQueryBuilder("age");
        queryBuilder.gte("10");

        BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();//组合查询
        boolQueryBuilder.must(QueryBuilders.matchQuery("name","zx"));//必须符合
        boolQueryBuilder.should(QueryBuilders.termQuery("age","11"));//可能符合
        boolQueryBuilder.mustNot(QueryBuilders.matchQuery("name","zxx"));//必须不符合
        boolQueryBuilder.should(queryBuilder);
        NativeSearchQuery searchQuery=new NativeSearchQueryBuilder()
                .withQuery(boolQueryBuilder)
                //分页
                .withPageable(PageRequest.of(0, 5))
                //排序
//                .withSort(SortBuilders.fieldSort("id").order(SortOrder.DESC))
                //高亮字段显示
                .withHighlightFields(new HighlightBuilder.Field("zx"))
                .build();

        restTemplate.queryForList(searchQuery, EmployeeBean.class);
        return null;
    }

    @PostMapping("/estest")
    public List<EmployeeBean> test(){


//        UpdateRequest updateRequest=new UpdateRequest();
//        updateRequest.index("").id("");
//        updateRequest.doc
//        restTemplate.update();
        String name="zx";
        int value=1;
        //关键字匹配
        TermQueryBuilder queryBuilder=new TermQueryBuilder(name, value);


        BoolQueryBuilder boolQueryBuilder =QueryBuilders.boolQuery();
        boolQueryBuilder.must(QueryBuilders.termsQuery("name", "zx"));
        boolQueryBuilder.filter(boolQueryBuilder);

        NativeSearchQuery nativeSearchQuery = new NativeSearchQueryBuilder()
                //查询条件
                .withQuery(boolQueryBuilder)
                //分页
                .withPageable(PageRequest.of(0, 5))
                //排序
//                .withSort(SortBuilders.fieldSort("id").order(SortOrder.DESC))
                //高亮字段显示
                .withHighlightFields(new HighlightBuilder.Field("zx"))
                .build();

        List<EmployeeBean> list= restTemplate.queryForList(nativeSearchQuery, EmployeeBean.class);
        return list;
//        restTemplate.query(queryBuilder)


        //分页查询
//        MatchQueryBuilder builder = QueryBuilders.matchQuery("type", "1");
//        SearchQuery searchQuery = new NativeSearchQuery(builder).setPageable(PageRequest.of(0, 1));
//        AggregatedPage<EmployeeBean> page = restTemplate.queryForPage(searchQuery, EmployeeBean.class);
//        long totalElements = page.getTotalElements(); // 总记录数
//        int totalPages = page.getTotalPages();  // 总页数
//        int pageNumber = page.getPageable().getPageNumber(); // 当前页号
//        List<EmployeeBean> beanList = page.toList();  // 当前页数据集
//        Set<EmployeeBean> beanSet = page.toSet();  // 当前页数据集
//        System.out.println(page);
        //查询
//        MatchQueryBuilder builder = QueryBuilders.matchQuery("type", "1");
//        SearchQuery searchQuery = new NativeSearchQuery(builder);
//        List<EmployeeBean> list= restTemplate.queryForList(searchQuery, EmployeeBean.class);
//        System.out.println(list);

//        /**
//         * 批量修改
//         * @param indexName 索引名称
//         * @param type      索引类型
//         * @param beanList 修改对象集合
//         */public void batchUpdate(String indexName, String type, List<EmployeeBean> beanList) {
//            List<UpdateQuery> queries = new ArrayList<>();
//            UpdateQuery updateQuery;
//            UpdateRequest updateRequest;
//            int counter = 0;
//            for (EmployeeBean item : beanList) {
//                updateRequest = new UpdateRequest();
//                updateRequest.retryOnConflict(1);//冲突重试
//                updateRequest.doc(item);
//                updateRequest.routing(item.getId());
//
//                updateQuery = new UpdateQuery();
//                updateQuery.setId(item.getId());
//                updateQuery.setDoUpsert(true);
//                updateQuery.setUpdateRequest(updateRequest);
//                updateQuery.setIndexName(indexName);
//                updateQuery.setType(type);
//                queries.add(updateQuery);
//                //分批提交索引
//                if (counter != 0 && counter % 1000 == 0) {
//                    restTemplate.bulkUpdate(queries);
//                    queries.clear();
//                    System.out.println("bulkIndex counter : " + counter);
//                }
//                counter++;
//            }
//            //不足批的索引最后不要忘记提交
//            if (queries.size() > 0) {
//                restTemplate.bulkUpdate(queries);
//            }
//            restTemplate.refresh(indexName);

        //批量新增
//        List<IndexQuery> queries = new ArrayList<>();
//        IndexQuery indexQuery;
//        int counter = 0;
//        for (EmployeeBean item : beanList) {
//            indexQuery = new IndexQuery();
//            indexQuery.setId(item.getId());
//            indexQuery.setSource(JSONUtil.toJsonStr(item));
//            indexQuery.setIndexName(indexName);
//            indexQuery.setType(type);
//            queries.add(indexQuery);
//            //分批提交索引
//            if (counter != 0 && counter % 1000 == 0) {
//                restTemplate.bulkIndex(queries);
//                queries.clear();
//                System.out.println("bulkIndex counter : " + counter);
//            }
//            counter++;
//        }
//        //不足批的索引最后不要忘记提交
//        if (queries.size() > 0) {
//            restTemplate.bulkIndex(queries);
//        }
//        restTemplate.refresh(indexName);










//        //根据id删除
//        repository.deleteById("1");
//        //根据id删除
//        repository.delete(bean);
//        //根据对象list
//        repository.deleteAll(beanList);
//        //删除所有
//        repository.deleteAll();
//        //条件删除
//        DeleteQuery deleteQuery = new DeleteQuery();
//        deleteQuery.setIndex(indexName);
//        deleteQuery.setType(type);//建index没配置就是类名全小写
//        deleteQuery.setQuery(new BoolQueryBuilder().must(QueryBuilders.termQuery("mobile","13526568454")));
//        restTemplate.delete(deleteQuery);



        //修改
//        UpdateRequest updateRequest = new UpdateRequest();
//        updateRequest.retryOnConflict(1);//冲突重试
//        updateRequest.doc(JSONUtil.toJsonStr(bean), XContentType.JSON);
//        updateRequest.routing(bean.getId());//默认是_id来路由的，用来路由到不同的shard，会对这个值做hash，然后映射到shard。所以分片
//        UpdateQuery query = new UpdateQueryBuilder().withIndexName(indexName).withType(type).withId(bean.getId())
//                .withDoUpsert(true)//不加默认false。true表示更新时不存在就插入
//                .withClass(EmployeeBean.class).withUpdateRequest(updateRequest).build();
//        UpdateResponse updateResponse = restTemplate.update(query);

//        保存数据
//        EmployeeBean bean=new EmployeeBean();
//        bean.setId("1");
//        bean.setName("zxzx");
//        bean.setMobile("1234");
//        bean.setType(1);
//        bean.setDesc("zxzxzx");
//        bean.setStudentCode("111222");
//        List<MultiLanguageModel> list=new ArrayList<>();
//        MultiLanguageModel model=new MultiLanguageModel();
//        model.setName("zx");
//        model.setLanguage("cn");
//        model.setCode("11");
//        list.add(model);
//        bean.setMultiLanguageSkuNameList(list);
//        repository.save(bean);

//保存所有的数据
//        repository.saveAll(list);

        //判断索引是否存在
//        boolean flag=restTemplate.indexExists(EmployeeBean.class);
//        System.out.println(flag);
//        boolean flag2= restTemplate.indexExists("employee_index");
//        System.out.println(flag2);

        //        //创建索引
//        boolean createflag= restTemplate.createIndex(EmployeeBean.class);
//        System.out.println(createflag);
//        //删除
//        restTemplate.deleteIndex(indexName);
    }

}
