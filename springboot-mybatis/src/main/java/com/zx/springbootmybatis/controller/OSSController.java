package com.zx.springbootmybatis.controller;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.zx.springbootmybatis.error.CommonErrorEnum;
import com.zx.springbootmybatis.exception.CommonException;
import com.zx.springbootmybatis.model.User;
import com.zx.springbootmybatis.util.FileErrorEnum;
import com.zx.springbootmybatis.util.FileUtil;
import com.zx.springbootmybatis.util.OssUtil;
import lombok.extern.log4j.Log4j;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.ClassUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

@RestController
@Log4j2
public class OSSController {
    @Autowired
    OssUtil ossUtil;

    @Value("${product.image.format:?x-oss-process=image/resize,m_fixed,}")
    private String imageFormat;




    @Value("${import.upload.dir:upload/}")
    private String importUploadDir;
    //直接上传
    @PostMapping( "/img")
    public String uploadImg(@RequestParam MultipartFile file){
        String s = ossUtil.uploadImg2Oss(file);
        return s;
    }

    //解压上传
    @PostMapping( "/imgzip")
    public String uploadImgZip(@RequestParam MultipartFile multipartFile){
        if (null == multipartFile) {
            throw new CommonException(FileErrorEnum.ERROR_IMPORT_FILE_EMPTY);
        }
        if (!multipartFile.getOriginalFilename().endsWith( ".zip")) {
            throw new CommonException(FileErrorEnum.ERROR_IMPORT_FILE_TYPE);
        }

        //获取服务器相对路径
        String path = ClassUtils.getDefaultClassLoader().getResource("").getPath() + importUploadDir;
        FileOutputStream outputStream = null;
        try {
            String zipPath = path + multipartFile.getOriginalFilename();
            File targetFile = new File(path);
            if (!targetFile.exists()) {
                targetFile.mkdirs();
            }
            outputStream = new FileOutputStream(zipPath);
            log.info("开始写入zip文件：{}", zipPath);
            outputStream.write(multipartFile.getBytes());
            List<String> txtPartPathList = FileUtil.unZipFiles(zipPath, path);
            if (CollectionUtils.isEmpty(txtPartPathList)) {
                throw new CommonException(FileErrorEnum.ERROR_IMPORT_TYPE);
            }
            txtPartPathList.stream().forEach(txtPartPath->{
                String txtPath = path + txtPartPath;

                String dirSrc = txtPath.substring(txtPath.lastIndexOf("/"),txtPath.length() );

                ossUtil.uploadImgurl(txtPath, dirSrc);
            });


        } catch (IOException e) {
            log.error("*****zip文档写入失败*****", e);
            throw new CommonException(FileErrorEnum.ERROR_IMPORT_FILE);
        } finally {
            try {
//
//                FileUtil.deleteFile(importInfo.getZipPath());
                if (null != outputStream) {
                    outputStream.flush();
                    outputStream.close();
                }
                FileUtil.deleteFile(path);

            } catch (IOException e) {
                log.error("*****outputStreamWriter流close失败*****", e);
                throw new CommonException(FileErrorEnum.ERROR_IMPORT_FILE);
            }
        }


        return "success";
    }


    public static void main(String[] args) {
        String a="1/1";
        String b=".*/.*";
        System.out.println(a.matches(b));

        String str="2233/40832-1-1200Wx1200H_100Wx100H.png";

        String regex="([1-9][0-9]{4,6})(\\-)([0-9]{1})(\\-.*)((.jpg)|(.png))";

        String[] split = str.split("/");
        System.out.println(split[0]+split[1]);

        String prestr=split[0];
        String head =prestr.substring(prestr.length() - 2, prestr.length());
        System.out.println("head:"+head);

        String tailstr=split[1];
        String tail =tailstr.substring(tailstr.indexOf("-")+1, tailstr.lastIndexOf("-"));
        System.out.println("tail:"+tail);

        boolean matches = tailstr.matches(regex);
        System.out.println("matches:"+matches);
        String fileDir=head+"/"+prestr+"/"+tail+"/"+tailstr;

        System.out.println("fileDir:"+fileDir);


    }



    public static void main2(String[] args) throws IOException {

        //获取文件输入流
        FileInputStream input = new FileInputStream("C:\\Users\\xing.zhang\\Desktop\\新建文件夹 (4).zip");

        //获取ZIP输入流(一定要指定字符集Charset.forName("GBK")否则会报java.lang.IllegalArgumentException: MALFORMED)
        ZipInputStream zipInputStream = new ZipInputStream(new BufferedInputStream(input), Charset.forName("GBK"));

        //定义ZipEntry置为null,避免由于重复调用zipInputStream.getNextEntry造成的不必要的问题
        ZipEntry ze = null;
        ArrayList<String> activityFile = new ArrayList<>();
        try {
        //循环遍历
        while ((ze = zipInputStream.getNextEntry()) != null) {
            String zipEntryName = ze.getName();
            if( zipEntryName.endsWith(".jpg")|| zipEntryName.endsWith(".png")){
                System.out.println("文件名：" + ze.getName() + " 文件大小：" + ze.getSize() + " bytes");
                activityFile.add( zipEntryName);
            }
//            System.out.println("文件内容：");
            //读取
//            BufferedReader br = new BufferedReader(new InputStreamReader(zipInputStream,Charset.forName("GBK")));
//
//            String line;

//            //内容不为空，输出
//            while ((line = br.readLine()) != null) {
//                System.out.println(line);
//            }
        }


        }catch (Exception e){
            throw new CommonException(FileErrorEnum.ERROR_IMPORT_FILE);
        }finally {
            //一定记得关闭流
            zipInputStream.closeEntry();
            input.close();
//            try {
//                if( null != zip ){
//                    zip.close();
//                }
//            }catch (Exception e){
//                log.error("关闭zip文件流{}失败", descDir);
//            }
        }
    }














}
