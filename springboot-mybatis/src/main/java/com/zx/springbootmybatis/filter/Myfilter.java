package com.zx.springbootmybatis.filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;
//过滤器
public class Myfilter implements Filter {
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        System.out.println("-------------------进入过滤器--------------------------");
        filterChain.doFilter(servletRequest,servletResponse);
    }

}
