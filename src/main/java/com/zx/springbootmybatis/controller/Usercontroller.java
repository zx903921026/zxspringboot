package com.zx.springbootmybatis.controller;

import com.zx.springbootmybatis.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Usercontroller {
    @Autowired
    StringRedisTemplate stringRedisTemplate;

    @Autowired
    RedisTemplate redisTemplate;

    @Autowired
    User user;

    @GetMapping("/redis/user")
    public User test1(){
        System.out.println(user.getName()+user.getAge());
        redisTemplate.opsForValue().set("a",user);
        return (User) redisTemplate.opsForValue().get("a");
    }

    //类似于hashset  不能重复
    @GetMapping("/redis/redisSet")
    public String redisSet(){
        String key="redis-set-key";
        String value="zx";
        stringRedisTemplate.opsForSet().add(key,value+"1");
        stringRedisTemplate.opsForSet().add(key,value+"2");
        stringRedisTemplate.opsForSet().add(key,value+"3");
        return stringRedisTemplate.opsForSet().pop(key);
    }
    //可用于消息队列
    @GetMapping("/redis/redisList")
    public String redisList(){
        String key="redis-list-key";
        String value="zx";
        stringRedisTemplate.opsForList().leftPush(key,value+"1");
        stringRedisTemplate.opsForList().leftPush(key,value+"2");
        stringRedisTemplate.opsForList().leftPush(key,value+"3");
        return stringRedisTemplate.opsForList().leftPop(key);
    }












}
