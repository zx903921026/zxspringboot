package com.example.springboot001.jedis;

import com.alibaba.fastjson.JSON;
import org.junit.Test;
import org.springframework.util.SerializationUtils;
import redis.clients.jedis.Jedis;

import java.util.Date;

public class Demo1 {
    @Test
   public void get (){
        Jedis jedis=new Jedis("1.116.45.81",7001);

        // 存储对象 - 以byte[]形式存储在Redis中
        //2.1 准备key(String)-value(User)
        String key = "user";
        User value = new User(1,"张三",new Date());
        //2.2 将key和value转换为byte[]
        byte[] byteKey = SerializationUtils.serialize(key);
        byte[] byteValue = SerializationUtils.serialize(value);
        //2.3 将key和value存储到Redis
        jedis.set(byteKey,byteValue);

        //2.3 jedis去Redis中获取value
        byte[] value1 = jedis.get(byteKey);
        //2.4 将value反序列化为User对象
        User user = (User) SerializationUtils.deserialize(value1);
        //2.5 输出
        System.out.println("user:" + user);


        jedis.close();
    }

    // 存储对象 - 以String形式存储
    @Test
    public void setString(){
        //1. 连接Redis
        Jedis jedis=new Jedis("1.116.45.81",7001);
        //2.1 准备key(String)-value(User)
        String stringKey = "stringUser";
        User value = new User(2,"李四",new Date());
        //2.2 使用fastJSON将value转化为json字符串
        String stringValue = JSON.toJSONString(value);
        //2.3 存储到Redis中
        jedis.set(stringKey,stringValue);

        String key = "stringUser";
        //2.2 去Redis中查询value
        String value2 = jedis.get(key);
        //2.3 将value反序列化为User
        User user = JSON.parseObject(value2, User.class);
        //2.4 输出
        System.out.println("user:" + user);


        //3. 释放资源
        jedis.close();
    }















}
