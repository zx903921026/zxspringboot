package com.example.springboot001.jedis;

import org.junit.Test;
import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.JedisCluster;

import java.util.HashSet;
import java.util.Set;

public class Demo5 {

    @Test
    public void test(){
        // 创建Set<HostAndPort> nodes
        Set<HostAndPort> nodes = new HashSet<>();
        nodes.add(new HostAndPort("1.116.45.81",7001));
        nodes.add(new HostAndPort("1.116.45.81",7002));
        nodes.add(new HostAndPort("1.116.45.81",7003));


        // 创建JedisCluster对象
        JedisCluster jedisCluster = new JedisCluster(nodes);

        // 操作
        String value = jedisCluster.get("b");
        System.out.println(value);
    }
}
