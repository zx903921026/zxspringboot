package com.example.springboot001.config;

import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitmqConfig {
//    @Value("${mq.queue.name}")
//    private String queueName;
    //队列的配置文件
    @Bean
    public Queue createQueue(){
        return new Queue("que-webm-cc-product-label-001");
    }
}
