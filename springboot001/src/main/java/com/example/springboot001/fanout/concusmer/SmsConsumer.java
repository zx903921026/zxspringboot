package com.example.springboot001.fanout.concusmer;//package com.zx.springbootmybatis.mq.fanout.concusmer;
//
//import com.rabbitmq.client.Channel;
//import org.springframework.amqp.core.ExchangeTypes;
//import org.springframework.amqp.core.Message;
//import org.springframework.amqp.rabbit.annotation.*;
//import org.springframework.stereotype.Component;
//
//import java.io.IOException;
//
//@Component
//@RabbitListener(bindings = @QueueBinding
//        (value = @Queue(value = "${mq.config.fanout.queue.sms}",autoDelete = "true")
//                ,exchange = @Exchange(value = "${mq.config.fanout.exchange}",
//                type = ExchangeTypes.FANOUT))
//)
//public class SmsConsumer {
//
//    @RabbitHandler
//    public void process(String msg, Channel channel, Message message) throws IOException {
//        System.out.println("SmsConsumer消费消息 :"+msg);
//        channel.basicAck(message.getMessageProperties().getDeliveryTag(),false);
//    }
//}
