package com.example.springboot001.fanout.provider;

import com.alibaba.fastjson.JSONObject;
import com.amway.bpaas.infrastructure.api.kinesis.KinesisMessage;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * 消息提供者
 */
@Component
public class LianweiFanoutProvider {
    @Autowired
    AmqpTemplate template;

//    @Value("${mq.config.fanout.exchange}")
//    String exchange;



    public void send(KinesisMessage msg){
        template.convertAndSend("xch-fanout-webm-cc-product-label","" ,msg);
    }


}
