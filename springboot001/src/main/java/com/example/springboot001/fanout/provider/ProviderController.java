package com.example.springboot001.fanout.provider;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.amway.bpaas.infrastructure.api.kinesis.KinesisMessage;
import com.amway.bpaas.infrastructure.api.mq.model.MqPayload;
import com.rabbitmq.tools.json.JSONUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.amway.bpaas.infrastructure.consts.InterfaceType.MQ_SEND;

@RestController
public class ProviderController {
    @Autowired
    LianweiFanoutProvider fanoutProvider;

        @PostMapping("/send")
        public void send(){
//            String a="{\\n\" +\n" +
//                    "                    \"  \\\"uuid\\\" : \\\"product-label_20210901111635\\\",\\n\" +\n" +
//                    "                    \"  \\\"type\\\" : \\\"MQ_SEND\\\",\\n\" +\n" +
//                    "                    \"  \\\"data\\\" : {\\n\" +\n" +
//                    "                    \"    \\\"class\\\" : \\\"com.amway.bpaas.infrastructure.api.kinesis.KinesisMessage\\\",\\n\" +\n" +
//                    "                    \"    \\\"sender\\\" : \\\"WM\\\",\\n\" +\n" +
//                    "                    \"    \\\"timestamp\\\" : \\\"2021-09-01 11:16:37:729\\\",\\n\" +\n" +
//                    "                    \"    \\\"messageId\\\" : \\\"product-label_20210901111635\\\",\\n\" +\n" +
//                    "                    \"    \\\"messageType\\\" : \\\"product-label\\\",\\n\" +\n" +
//                    "                    \"    \\\"messageDetail\\\" : \\\"{\\\\\\\"regionCode\\\\\\\":\\\\\\\"130\\\\\\\",\\\\\\\"bizCode\\\\\\\":\\\\\\\"INT\\\\\\\",\\\\\\\"useCase\\\\\\\":\\\\\\\"\\\\\\\",\\\\\\\"scenario\\\\\\\":\\\\\\\"\\\\\\\",\\\\\\\"language\\\\\\\":\\\\\\\"\\\\\\\",\\\\\\\"timeZone\\\\\\\":\\\\\\\"GMT+8\\\\\\\",\\\\\\\"extendProperty\\\\\\\":\\\\\\\"\\\\\\\",\\\\\\\"updateTime\\\\\\\":\\\\\\\"20210901111635\\\\\\\",\\\\\\\"sourceFrom\\\\\\\":\\\\\\\"WM\\\\\\\",\\\\\\\"itemCodes\\\\\\\":[\\\\\\\"2029\\\\\\\",\\\\\\\"2644\\\\\\\"],\\\\\\\"tagCode\\\\\\\":\\\\\\\"AO\\\\\\\",\\\\\\\"channelCode\\\\\\\":\\\\\\\"DEFAULT\\\\\\\",\\\\\\\"tagRole\\\\\\\":\\\\\\\"\\\\\\\"}\\\"\\n\" +\n" +
//                    "                    \"  }\\n\" +\n" +
//                    "                    \"}";
            MqPayload<KinesisMessage> mqPayload=new MqPayload<>();
            mqPayload.setUuid("product-label_20210901111635");
            mqPayload.setType(MQ_SEND);

            KinesisMessage message=new KinesisMessage();
            message.setMessageId("product-label_20210901111635");
            message.setSender("WM");
            message.setMessageType("product-label");

            JSONObject data=new JSONObject();
            String a="{\n" +
                    "\t\"regionCode\": \"130\",\n" +
                    "\t\"bizCode\": \"INT\",\n" +
                    "\t\"useCase\": \"\",\n" +
                    "\t\"scenario\": \"\",\n" +
                    "\t\"language\": \"\",\n" +
                    "\t\"timeZone\": \"GMT+8\",\n" +
                    "\t\"extendProperty\": \"\",\n" +
                    "\t\"updateTime\": \"20210831173310\",\n" +
                    "\t\"sourceFrom\": \"WM\",\n" +
                    "\t\"itemCodes\": [\n" +
                    "\t\t\"2029\",\n" +
                    "\t\t\"2644\",\n" +
                    "\t\t\"8737\"\n" +
                    "\t],\n" +
                    "\t\"tagCode\": \"AO\",\n" +
                    "\t\"channelCode\": \"DEFAULT\",\n" +
                    "\t\"tagRole\": \"\"\n" +
                    "}";
            JSONObject object=JSONObject.parseObject(a);

            message.setMessageDetail(a);


            mqPayload.setData(message);


            fanoutProvider.send( message);

            System.out.println("fanoutProvider发送成功");
        }

}
